<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }

    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-20);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $tglPembuatan = $this->ubahTanggal(date("Y-m-d")).", ".date("H:i:s")." WIB";

        $this->SetFont('helvetica', 'I', 8); 
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        // $this->MultiCell(0, 10, 'Halaman '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        // $this->MultiCell(0, 0, 'Tanggal pembuatan '.$tglPembuatan, 0, 1, 'L', 0, '', 0);
        $this->MultiCell(100, 0, 'Tanggal pembuatan '.$tglPembuatan, 0, 'L', 0, 0, '', '', true);
         //Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
        
        $this->Cell(0, 0, 'Halaman '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 1, 'R', 0, '', 1);
        $this->writeHTML("<hr>", true, false, true, false, '');
        $this->MultiCell(100, 0, 'KOPERASI TASS INDONESIA NUSANTARA', 0, 'L', 0, 0, '', '', true);
        $this->Cell(0, 0, 'Jl. Ridwan Rais No. 18-22 Beji Timur Depok (16422)', 0, 1, 'R', 0, '', 0);
        $this->Cell(0, 0, 'Telp. 021-77201178 Fax. 021-77203599', 0, 1, 'R', 0, '', 1);
        $this->Cell(0, 0, 'www.koptassindo.com', 0, 1, 'R', 0, '', 1);
       
    }

    public function Header(){
        $this->Image(base_url("assets/pages/img/login/kopHeader.png"), 15, 10, 25, 23, 'PNG', '', 'left', true, 150, '', false, false, 0, false, false, false);
        $this->ln(0);
        
    }

    public function ubahTanggal($date){
        $hasil="";
        $bln= "";
        $tahun= substr($date,0,4);
        $bulan= substr($date,5,2);
        $tgl= substr($date,8,2);

        switch ($bulan) {
            case '01':
                $bln = "Januari";
            break;
            case '01':
                $bln = "Januari";
            break;
            case '02':
                $bln = "Februari";
            break;
            case '03':
                $bln = "Maret";
            break;
            case '04':
                $bln = "April";
            break;
            case '05':
                $bln = "Mei";
            break;
            case '06':
                $bln = "Juni";
            break;
            case '07':
                $bln = "Juli";
            break;
            case '08':
                $bln = "Agustus";
            break;
            case '09':
                $bln = "September";
            break;
            case '10':
                $bln = "Oktober";
            break;
            case '11':
                $bln = "November";
            break;
            case '12':
                $bln = "Desember";
            break;
            
            default:
                $bln = $bulan;
            break;
        }

        $hasil = $tgl." ".$bln." ".$tahun;

        return $hasil;
    }
}