<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TokenAPI {

	private $tokenList;
	public $keyProtect = "Garuda steel hcms app!@#";

	public function __construct() {
		$this->tokenList = array(
			'cpybGN6McxUgzAzmapbhNZjKQM5Z7K',
			'5HYoo7gDCWkZQq0oGfDkcE4L1mEacz',
			'eun18eBVdlvZPi9sN4a1t7BUIp5QQp',
			'H2I72eFXcByWLUHvQmYC0EHMn2JVxc',
			'pyzozB5iAyaagooIXzT8w3faijx28k',
			'mrlyOBOYvfR2vBlJpnAvkNsmQygJ3U',
			'TByxFrrHqdi5wodVNWNVAk8kKxPPC1',
			'XE2K8wmcqFpksMyCi6C1ylY1jJqAbX',
			'lbehyL5oqPNvMj0mYILaPZHEIBS0tw',
			'UYrdTMsFp7RkyewbUOKGB6pOcOQ2Oa',
			'Tj1RU8U9Ir6JGIFOCVd8UxOUnw5Rlm',
			'7cH0uDKVjom5adRYah7xSlrgpBH7lM',
			'eyN6SylhYA5wnBCzr62DY2VqmRnapz',
			'ZzuCRSqYAMz7fwM0rNn6hQdoknDlIB',
			'uUZf6zixlhbbxxLoxhEUGZTWPD7ciH',
			'qUW7nN7o64EsarTPF9GqdTnKTUOLiU',
			'kVv6Mc9du5htkwwACZWLcraKrngKbK',
			'PtJ1YaYahvfEQKnrfBI1Bg6luuTT0t',
			'UCKVwZO6y0NGn5LUNflubWCDm7b6Hg',
			'831m2gGQ1UwIGJSp5WBVPf1I1T0Lgz',
			'TwCwvYIsdzCAQiWEKBLQLuGTfoN12i',
			'IWsEuSnsKx7aA9yKzr0GOCkUUpQVX1',
			'qGxaoFJ5CPaupEVAD5A7DCfhfdwNOl',
			'tTcXJhIymbe4Xj4ZDU5iYFDr3Oxov3',
			'aCnDk6igpim9db816dJ5VnJ518fddr'
		);
	}

	public function getToken(){
		$singleToken = array_rand($this->tokenList, 1);
		$token = $this->tokenList[$singleToken];

		return $token;
	}

	public function checkToken($token){
		$exist = in_array($token, $this->tokenList);
		return $exist;
	}

}