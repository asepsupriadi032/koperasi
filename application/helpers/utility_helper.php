<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function angStatus($status){

	if($status == "1"){
		$statusAng = "Terverifikasi";
	}elseif($status == '2'){
		$statusAng = "Menunggu Persetujuan";
	}elseif($status == '3'){
		$statusAng = "Ditolak";
	}elseif($status == '4'){
		$statusAng = "Suspend";
	}elseif($status == '0'){
		$statusAng = "Terminasi";
	}else{
		$statusAng = "-";
	}
 
	return $statusAng;
}

function statusTransaksi($status){
	if($status == "0"){
		$statusTransaksi = "Belum Lunas";
	}elseif($status == "1"){
		$statusTransaksi = "lunas";
	}elseif($status == "2"){
		$statusTransaksi = "-";
	}else{
		$statusTransaksi = "-";
	}

	return $statusTransaksi;
}

function statusOperational($status){
	if($status == "1"){
		$statusOperational = "Menunggu transfer";
	}elseif ($status == "2") {
		$statusOperational = "Ditolak";
	}elseif ($status == "3") {
		$statusOperational = "Sudah ditransfer";
	}elseif ($status == "4") {
		$statusOperational = "Ditinjau ulang";
	}elseif ($status == "5") {
		$statusOperational = "Dibatalkan anggota";
	}else{
		$statusOperational = "-";
	}

	return $statusOperational;
}

function katProduk($kategori){
	if($kategori=="1"){
		$katProduk = "Grup";
	}elseif($kategori=="2"){
		$katProduk = "Personal";
	}else{
		$katProduk = "-";
	}

	return $katProduk;
}

function getTanggal($date){

	$hasil="";
    $bln= "";
    $tahun= substr($date,0,4);
    $bulan= substr($date,5,2);
    $tgl= substr($date,-2);

    switch ($bulan) {
        case '01':
            $bln = "Januari";
        break;
        case '01':
            $bln = "Januari";
        break;
        case '02':
            $bln = "Februari";
        break;
        case '03':
            $bln = "Maret";
        break;
        case '04':
            $bln = "April";
        break;
        case '05':
            $bln = "Mei";
        break;
        case '06':
            $bln = "Juni";
        break;
        case '07':
            $bln = "Juli";
        break;
        case '08':
            $bln = "Agustus";
        break;
        case '09':
            $bln = "September";
        break;
        case '10':
            $bln = "Oktober";
        break;
        case '11':
            $bln = "November";
        break;
        case '12':
            $bln = "Desember";
        break;
        
        default:
            $bln = $bulan;
        break;
    }

    $hasil = $tgl." ".$bln." ".$tahun;

    return $hasil;
}

