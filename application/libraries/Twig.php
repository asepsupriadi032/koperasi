<?php
/**
 * Part of CodeIgniter Simple and Secure Twig
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/codeigniter-ss-twig
 */

// If you don't use Composer, uncomment below

require_once APPPATH . 'third_party/Twig-1.27.0/lib/Twig/Autoloader.php';
Twig_Autoloader::register();


class Twig
{
	private $config = [];

	private $functions_asis = [
		'base_url', 'site_url'
	];
	private $functions_safe = [
		'form_open', 'form_close', 'form_error', 'set_value', 'form_hidden'
	];

	/**
	 * @var bool Whether functions are added or not
	 */
	private $functions_added = FALSE;

	/**
	 * @var Twig_Environment
	 */
	private $twig;

	/**
	 * @var Twig_Loader_Filesystem
	 */
	private $loader;
	
	private $CI;	
	private $template_module_dir;
	protected $_twig;
    protected $_twig_loader;

	public function __construct($params = [])
	{

		$this->CI =& get_instance();
		$this->CI->load->library('session');

		$this->template_module_dir = APPPATH.'modules/'.$this->CI->router->fetch_module().'/views/';	
		
		// default config
		$this->config = [
			'paths' => [$this->template_module_dir, VIEWPATH],
			// 'cache' => APPPATH . 'cache/twig',
			'cache' => FALSE,
		];
		

		$this->config = array_merge($this->config, $params);
		if (isset($params['functions']))
		{
			$this->functions_asis = 
				array_unique(
					array_merge($this->functions_asis, $params['functions'])
				);
		}
		if (isset($params['functions_safe']))
		{
			$this->functions_safe = 
				array_unique(
					array_merge($this->functions_safe, $params['functions_safe'])
				);
		}
		

		$this->addGlobal("base_url", $this->CI->config->base_url());
		$this->addGlobal("menu", $this->getMenuItem());
		$this->addGlobal("session", $this->CI->session);
		$this->addGlobal("usr_id", $this->getusrid());

	
	}

	protected function resetTwig()
	{
		$this->twig = null;
		$this->createTwig();
	}

	protected function createTwig()
	{
		// $this->twig is singleton
		if ($this->twig !== null)
		{
			return;
		}

		if (ENVIRONMENT === 'production')
		{
			$debug = FALSE;
		}
		else
		{
			$debug = TRUE;
		}

		if ($this->loader === null)
		{
			$this->loader = new \Twig_Loader_Filesystem($this->config['paths']);
		}

		$twig = new \Twig_Environment($this->loader, [
			'cache'      => $this->config['cache'],
			'debug'      => $debug,
			'autoescape' => TRUE,
		]);

		if ($debug)
		{
			$twig->addExtension(new \Twig_Extension_Debug());
		}

		$this->twig = $twig;
	}

	protected function setLoader($loader)
	{
		$this->loader = $loader;
	}

	/**
	 * Registers a Global
	 * 
	 * @param string $name  The global name
	 * @param mixed  $value The global value
	 */
	public function addGlobal($name, $value)
	{
		$this->createTwig();
		$this->twig->addGlobal($name, $value);
	}

	/**
	 * Renders Twig Template and Set Output
	 * 
	 * @param string $view   Template filename without `.twig`
	 * @param array  $params Array of parameters to pass to the template
	 */
	public function display($view, $params = [])
	{
		$CI =& get_instance();
		$CI->output->set_output($this->render($view, $params));
	}

	/**
	 * Renders Twig Template and Returns as String
	 * 
	 * @param string $view   Template filename without `.twig`
	 * @param array  $params Array of parameters to pass to the template
	 * @return string
	 */
	public function render($view, $params = [])
	{
		$this->createTwig();
		// We call addFunctions() here, because we must call addFunctions()
		// after loading CodeIgniter functions in a controller.
		$this->addFunctions();

		$view = $view;
		return $this->twig->render($view, $params);
	}

	protected function addFunctions()
	{
		// Runs only once
		if ($this->functions_added)
		{
			return;
		}

		// as is functions
		foreach ($this->functions_asis as $function)
		{
			if (function_exists($function))
			{
				$this->twig->addFunction(
					new \Twig_SimpleFunction(
						$function,
						$function
					)
				);
			}
		}

		// safe functions
		foreach ($this->functions_safe as $function)
		{
			if (function_exists($function))
			{
				$this->twig->addFunction(
					new \Twig_SimpleFunction(
						$function,
						$function,
						['is_safe' => ['html']]
					)
				);
			}
		}

		// customized functions
		if (function_exists('anchor'))
		{
			$this->twig->addFunction(
				new \Twig_SimpleFunction(
					'anchor',
					[$this, 'safe_anchor'],
					['is_safe' => ['html']]
				)
			);
		}

		$this->functions_added = TRUE;
	}

	/**
	 * @param string $uri
	 * @param string $title
	 * @param array  $attributes [changed] only array is acceptable
	 * @return string
	 */
	public function safe_anchor($uri = '', $title = '', $attributes = [])
	{
		$uri = html_escape($uri);
		$title = html_escape($title);
		
		$new_attr = [];
		foreach ($attributes as $key => $val)
		{
			$new_attr[html_escape($key)] = html_escape($val);
		}

		return anchor($uri, $title, $new_attr);
	}

	/**
	 * @return \Twig_Environment
	 */
	public function getTwig()
	{
		$this->createTwig();
		return $this->twig;
	}

	public function getusrid(){
		$CI =& get_instance();
		$usrId = $CI->session->userdata("hcmIdUser");
		return $usrId;
	}

	
	public function getMenuItem(){
		$CI =& get_instance();
		$str = " ";
		$ustId = $CI->session->userdata("hcmUstId");
		$usrId = $CI->session->userdata("hcmIdUser");
	    $CI->db->order_by('mnu_menu.mnu_urut','ASC');
	    $CI->db->where('mnu_menu.mnu_status','1');
	    $CI->db->where('mnu_menu.mnu_parent','0');
	    if($ustId != '1'){
	    	$CI->db->where("mnu_menu_role.usr_id",$usrId);
	   		$CI->db->join("mnu_menu_role","mnu_menu_role.mnu_id=mnu_menu.mnu_id");
	   		
	    }
	    $tmp_modul=$CI->db->get('mnu_menu');
	    // var_dump($tmp_modul); exit();
	    //menu level 1
	    foreach($tmp_modul->result() as $modul){
	        $CI->db->order_by('mnu_menu.mnu_urut','ASC');
	        $CI->db->where('mnu_menu.mnu_status','1');
	        $CI->db->where('mnu_menu.mnu_parent',$modul->mnu_id);
	        if($ustId != '1'){
		    	$CI->db->where("mnu_menu_role.usr_id",$usrId);
		   		$CI->db->join("mnu_menu_role","mnu_menu_role.mnu_id=mnu_menu.mnu_id");
		   		
		    }
	        $tmp_menu=$CI->db->get('mnu_menu');

	        $jml_menu=$tmp_menu->num_rows();
        	if($modul->mnu_id=='1'){
                $disabled="class='list-group-item disabled'";
            }

            $className = "";
            $toggle = "";
            if($jml_menu > 0){
            	$className = "<span class='arrow '></span>";
            	$toggle = "nav-toggle";
            }

            $str.="
            <li class='nav-item'>
            <a href='".base_url($modul->mnu_url)."' class='nav-link ".$toggle."'>
		        <i class='".$modul->mnu_icon."'></i>
		        <span class='title'>".$modul->mnu_name."</span>
		        ". $className."
		    </a>

		    <ul class='sub-menu'>
        	
            ";

            foreach($tmp_menu->result() as $menu){

                $id_subMenu=$menu->mnu_id;
                $CI->db->order_by('mnu_menu.mnu_urut','ASC');
                $CI->db->where('mnu_menu.mnu_status','1');
                $CI->db->where('mnu_menu.mnu_parent',$id_subMenu);
                if($ustId != '1'){
			    	$CI->db->where("mnu_menu_role.usr_id",$usrId);
			   		$CI->db->join("mnu_menu_role","mnu_menu_role.mnu_id=mnu_menu.mnu_id");
			   		
			    }
                $tmp_subMenu=$CI->db->get('mnu_menu');

                $jml_subMenu=$tmp_subMenu->num_rows();

                $className = "nav-link";
                $toggle = "";
                if($jml_subMenu > 0){
                	$className = "arrow nav-toggle";
                	$toggle = "nav-toggle";
                }

                
                    $str.="
                    <li class='nav-item'>
	    				<a href='".base_url($menu->mnu_url)."' class='nav-link ".$toggle."'>
	                        <i class='icon-arrow-right'></i> ".$menu->mnu_name."
	                        <span class='".$className."'></span>
	                    </a>		            

		            <ul class='sub-menu'>";


		            foreach($tmp_subMenu->result() as $subMenu){
	                                                
                        $id_subSubMenu=$subMenu->mnu_id;
                        $CI->db->order_by('mnu_menu.mnu_urut','ASC');
                        $CI->db->where('mnu_menu.mnu_status','1');
                        $CI->db->where('mnu_menu.mnu_parent',$id_subSubMenu);
                        $tmp_subSubMenu=$CI->db->get('mnu_menu');

                        $jml_subSubMenu=$tmp_subSubMenu->num_rows();

                        $str.="
						<li class='nav-item'>
                            <a href='".base_url($subMenu->mnu_url)."' class='nav-link'>
                                <i class='fa fa-caret-right'></i> ".$subMenu->mnu_name."</a>
                        </li>";


                    }

                    $str.="</ul>

                    </li>";

                
            } 

            $str.="
            </ul></li>";   

	    }

	    $str.="<li class='heading'>
		        <h3 class='uppercase'><ics class='fa fa-star-half-o'></ics>FAVORITE</h3>
		    </li>";
		    $usr_id=$CI->session->userdata('hcmIdUser');
		    $CI->db->where('fav_user.usr_id',$usr_id);
		    $row=$CI->db->get('fav_user')->result();
		    // $str .= "<li class='nav-item' id='url_sementara'>";

		    foreach($row as $fav){

		    	// if(base_url($fav->fav_link)==$this->CI->config->base_url()){
		    		$this->addGlobal('url_active',$this->CI->config->base_url());
		    	// }
			    $str.=" <li class='nav-item'>
					        <a href='".base_url($fav->fav_link)."'>
					            <i class='fa fa-star'></i>
					            <span class='title'>".$fav->fav_name."</span>
					        </a>
					    </li>";
			}

			// $str.="</li>";
			$str .= "<li class='nav-item' id='url_sementara'></li>";

        
	    //var_dump($str);
	    //exit();

	    return $str;
	}
}
