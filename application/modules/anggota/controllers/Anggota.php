<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->load->helper('utility');	
		$this->container['data'] = null;
		
		// $this->API = $this->config->item('api_url')."/utility";
		$this->load->model('AnggotaModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridAnggota.html", $this->container);
	}

	public function getAjaxData($kat_id = NULL){
		$param['kat_id'] = $kat_id;
		$param['action'] = "anggota";
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->AnggotaModel->getAnggota($obj);
		// var_dump($kat_id); exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;			
				$responce->data[] = array(
					$x, 
					"<a href='".base_url()."produk/setProduk/DetailListProduk/".$row->kat_id."'>".$row->kat_kode."</a>", 
					"<a href='".base_url("anggota/Anggota/detailAnggota/".$row->ang_id)."'>".$row->ang_nama."</a>",
					$row->kar_pekerjaan,
					$row->ang_alamat,
					angStatus($row->ang_status), 
					$row->ang_id,
					$row->ang_status
				);
			}
		}		
		echo json_encode($responce);
	}	

	public function detailAnggota($ang_id = NULL){
		if(!empty($ang_id)){
			$param['ang_id']=$ang_id;
			$param['token'] = $this->tokenAPI->getToken();	
			$data = $this->AnggotaModel->getDetail($param);
			$this->container['id']=$data['row']->ang_id;
			$this->container['ang_nama']=$data['row']->ang_nama;
		}
		
		// var_dump($this->container); exit();
		$this->twig->display("form/formMasterAnggota.html", $this->container);
	}
	
	public function addAnggota($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			// $data =  json_decode($this->curl->simple_post($this->API.'/menu', $param, array(CURLOPT_BUFFERSIZE => 10))); 
			$obj = (object) $param;
			$result = $this->AnggotaModel->postAnggota($obj);
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			// var_dump($result); exit();
			redirect(base_url('anggota/Anggota.html'));
		}

		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaModel->getKatInstansi($obj);
		$this->container['kat_instansi'] = $data['row'];

		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->AnggotaModel->getMenu($obj);
			// $data = json_decode($this->curl->simple_get($this->API.'/menu', $param));
			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}
		// var_dump($result); exit();
	
		$this->twig->display("form/formAnggota.html", $this->container);
	}


	public function getInstansi(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaModel->getSubInstansi($obj);

		// $this->container['kat_instansi'] = $data['row'];
		echo json_encode(array("type" => $data['type'], "row"=>$data['row']));

	}

	public function deleteImgProfil(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaModel->deleteImgProfil($obj);
		// var_dump($data); exit();
		// $this->container['kat_instansi'] = $data['row'];
		echo json_encode(array("type" => $data['type'], "msg"=>$data['msg']));
	}

	public function deleteAnggota($ang_id=Null){
		$param['ang_id'] = $ang_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaModel->deleteAnggota($obj); 

		// var_dump($data); exit();

		$this->session->set_flashdata(array("type" => $data['type'], "msg" => $data['msg']));

        redirect(base_url('anggota/Anggota.html'));
	}

	public function delAnggota(){
		$param['ang_id'] = $this->input->post('ang_id');
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;

		$data = $this->AnggotaModel->delAnggota($obj);
		echo json_encode(array("type" => $data['type'], "msg" => $data['msg']));
		// var_dump($data); exit();
	}

	public function editAnggota(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaModel->editAnggota($obj);

		// var_dump($data); exit();
		echo json_encode(array("type" => $data['type'], "msg" => $data['msg']));
		// echo json_encode(array("type" => $data->type, "msg" => $data->msg));
		// var_dump($data); exit();
	}


}
