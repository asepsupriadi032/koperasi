<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnggotaBiodata extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model("AnggotaBiodataModel");

		LoggedSystem();		
	}

	public function index($ang_id = null)
	{	

		$param['ang_id'] = $ang_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaBiodataModel->getData($obj);
		$this->container['edit'] = $data['row'];
		if(!empty($ang_id)){
			$this->container['img_anggota']=$data['row']->ang_profil;
		}
		
		if(!empty($this->session->userdata('img_anggota'))){
			$this->container['img_anggota'] = $this->session->userdata("img_anggota");
		}

		$this->container['usr_id'] = $this->session->userdata("hcmIdUser");
		
		$this->twig->display("form/formBiodata.html", $this->container);
	}

	public function saveImg(){

		// Set path folder tempat menyimpan gambarnya
		$ang_id = $this->input->post('ang_id');
		$uploadDir = "./assets/imgAnggota/";
		if(is_uploaded_file($_FILES['img_anggota']['tmp_name'])){

			$uploadFile = $_FILES['img_anggota'];
			// Extract nama file
			$extractFile = pathinfo($uploadFile['name']);
			$size = $_FILES['img_anggota']['size']; //untuk mengetahui ukuran file
			$tipe = $_FILES['img_anggota']['type'];// untuk mengetahui tipe file
			//Dibawah ini adalah untuk mengatur format gambar yang dapat di uplada ke server.
			//Anda bisa tambahakan jika ingin memasukan format yang lain tergantung kebutuhan anda.
			$exts =array('image/jpeg','image/jpg','image/png');
			if(!in_array(($tipe),$exts)){

				$this->session->set_flashdata(array("type" => "error", "msg" =>"Sorry, you must be upload only file  and JPG"));

				redirect(base_url('anggota/Anggota/detailAnggota.html'));

			}elseif(($size !=0)&&($size>(1000*1024))){

				// dibawah ini script untuk mengatur ukuran file yang dapat di upload ke server
				$this->session->set_flashdata(array("type" => "error", "msg" =>"Sorry, file max. 1 MB"));
				redirect(base_url('anggota/Anggota/detailAnggota.html'));
			}else{

				$sameName = 0; // Menyimpan banyaknya file dengan nama yang sama dengan file yg diupload
				$handle = opendir($uploadDir);
				while(false !== ($file = readdir($handle))){ // Looping isi file pada directory tujuan
					// Apabila ada file dengan awalan yg sama dengan nama file di uplaod
					if(strpos($file,$extractFile['filename']) !== false)
					$sameName++; // Tambah data file yang sama
				}
			 
				/* Apabila tidak ada file yang sama ($sameName masih '0') maka nama file pakai 
				* nama ketika diupload, jika $sameName > 0 maka pakai format "namafile($sameName).ext */
				$newName = empty($sameName) ? $uploadFile['name'] : $extractFile['filename'].'('.$sameName.').'.$extractFile['extension'];

					if(move_uploaded_file($uploadFile['tmp_name'],$uploadDir.$newName)){
						$this->session->set_userdata("img_anggota", $newName);
						redirect(base_url('anggota/Anggota/detailAnggota/'.$ang_id.'.html'));
						// echo $this->session->userdata("img_anggota")." id= ".$ang_id;

					}else{
						echo json_encode(array("type" => "error", "msg" =>"Error", "name"=>""));
					}
			}
			
		}
	}

	public function addAnggota(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		// $data =  json_decode($this->curl->simple_post($this->API.'/menu', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		

		$uploadDir = "./assets/imgKTP/";
		if(is_uploaded_file($_FILES['ang_foto_ktp']['tmp_name'])){

			$uploadFile = $_FILES['ang_foto_ktp'];
			// Extract nama file
			$extractFile = pathinfo($uploadFile['name']);
			$size = $_FILES['ang_foto_ktp']['size']; //untuk mengetahui ukuran file
			$tipe = $_FILES['ang_foto_ktp']['type'];// untuk mengetahui tipe file
			//Dibawah ini adalah untuk mengatur format gambar yang dapat di uplada ke server.
			//Anda bisa tambahakan jika ingin memasukan format yang lain tergantung kebutuhan anda.
			$exts =array('image/jpeg','image/jpg','image/png');
			if(!in_array(($tipe),$exts)){

				$this->session->set_flashdata(array("type" => "error", "msg" =>"Sorry, you must be upload only file  and JPG"));

				redirect(base_url('anggota/Anggota/detailAnggota.html'));

			}elseif(($size !=0)&&($size>(1000*1024))){

				// dibawah ini script untuk mengatur ukuran file yang dapat di upload ke server
				$this->session->set_flashdata(array("type" => "error", "msg" =>"Sorry, file max. 1 MB"));
				redirect(base_url('anggota/Anggota/detailAnggota.html'));
			}else{

				$sameName = 0; // Menyimpan banyaknya file dengan nama yang sama dengan file yg diupload
				$handle = opendir($uploadDir);
				while(false !== ($file = readdir($handle))){ // Looping isi file pada directory tujuan
					// Apabila ada file dengan awalan yg sama dengan nama file di uplaod
					if(strpos($file,$extractFile['filename']) !== false)
					$sameName++; // Tambah data file yang sama
				}
			 
				/* Apabila tidak ada file yang sama ($sameName masih '0') maka nama file pakai 
				* nama ketika diupload, jika $sameName > 0 maka pakai format "namafile($sameName).ext */
				$newName = empty($sameName) ? $uploadFile['name'] : $extractFile['filename'].'('.$sameName.').'.$extractFile['extension'];

					if(move_uploaded_file($uploadFile['tmp_name'],$uploadDir.$newName)){
						$param['ang_foto_ktp']= $newName;
						$obj = (object) $param;
						$result = $this->AnggotaBiodataModel->postAnggota($obj);
						$this->session->unset_userdata('img_anggota');
						$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
						$angId= $result['angId'];
						// var_dump($angId); exit();
						redirect(base_url('anggota/Anggota/detailAnggota/'.$angId.'.html'));
					}else{
						echo json_encode(array("type" => "error", "msg" =>"Error", "name"=>""));
					}
			}
			
		}

		$obj = (object) $param;
		// var_dump($param); exit();
		$result = $this->AnggotaBiodataModel->postAnggota($obj);
		$this->session->unset_userdata('img_anggota');
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
		$angId= $result['angId'];
		// var_dump($angId); exit();
		redirect(base_url('anggota/Anggota/detailAnggota/'.$angId.'.html'));
	}


}
