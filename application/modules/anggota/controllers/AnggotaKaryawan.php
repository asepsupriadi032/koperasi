<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnggotaKaryawan extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();

		$this->load->model("AnggotaKaryawanModel");	
		$this->load->model("AnggotaModel");	

		LoggedSystem();		
	}

	public function index($ang_id = null)	{	
		$this->container['ang_id'] = $ang_id;

		$param['ang_id']=$ang_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->AnggotaKaryawanModel->getData($obj);
		$this->container['edit'] = $data['row'];
		// var_dump($data);

		$param2['token'] = $this->tokenAPI->getToken();
		$obj2 = (object) $param2;
		$data2 = $this->AnggotaKaryawanModel->getKatInstansi($obj2);
		$this->container['instansi'] = $data2['row'];

		$param3['token'] = $this->tokenAPI->getToken();
		$obj3 = (object) $param3;
		$data3 = $this->AnggotaKaryawanModel->getDataKategori($obj3);
		$this->container['kategoriKaryawan'] = $data3['row'];
		$this->twig->display("form/formAnggotaKaryawan.html", $this->container);
	}

	public function saveByAjax(){

		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		// $data =  json_decode($this->curl->simple_post($this->API.'/menu', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		$obj = (object) $param;
		$result = $this->AnggotaKaryawanModel->postAnggota($obj);
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));


        echo json_encode(array("type" => $result['type'], "msg" => $result['msg']));

	}


}
