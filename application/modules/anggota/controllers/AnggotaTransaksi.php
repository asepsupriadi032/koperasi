<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnggotaTransaksi extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('utility');	
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();

		$this->load->model("AnggotaKaryawanModel");	
		$this->load->model("AnggotaTransaksiModel");	

		LoggedSystem();		
	}

	public function index($ang_id = NULL)
	{			
		$this->container['ang_id'] = $ang_id;
		$this->twig->display("grid/gridAnggotaTransaksi.html", $this->container);
	}

	public function getAjaxData($ang_id=NULL){
		$param['token'] = $this->tokenAPI->getToken();
		$param['ang_id'] = $ang_id;	
		$obj = (object) $param;
		$data = $this->AnggotaTransaksiModel->getAnggotaTransaksi($obj);
		// var_dump($data); exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{
			
			foreach($data['row'] as $row) { 
				$x++;
				if($row->tran_status_operational !="3"){
					$status = "-";
				}else{
					$status = statusTransaksi($row->tran_status);
				}

				if($row->tran_status_operational=="2" || $row->tran_status_operational=="4"){
					$komen = ": <b>".$row->tran_komen."</b>";
				}else{
					$komen = "";
				}
				$responce->data[] = array(
					$row->tran_transaksi_id,
					$row->kat_kode,
					// number_format($row->ang_dana,2,',','.'),
					// number_format($row->ang_limit,2,',','.'),
					$row->tran_kebutuhan,
					number_format($row->tran_nominal,2,',','.'),
					$row->tran_tgl_pengajuan,
					$row->tran_tgl_transfer,
					$row->tran_bulan,
					number_format($row->tran_perbulan,2,',','.'),
					$row->tran_bunga,
					number_format($row->tran_total,2,',','.'),
					statusOperational($row->tran_status_operational).$komen,
					$status
				);
			}
		}		
		echo json_encode($responce);
	}

}
