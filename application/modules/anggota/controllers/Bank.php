<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model("BankAnggotaModel");

		LoggedSystem();		
	}

	public function index($ang_id = null){	
		$param['ang_id'] = $ang_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;

		$data = $this->BankAnggotaModel->getData($obj);
		$this->container['edit'] = $data['row'];
		// var_dump($this->container['edit']);

		$data2 = $this->BankAnggotaModel->getBank($obj);
		$this->container['bank'] = $data2['row'];

		$this->container['ang_id']=$ang_id;
		$this->twig->display("form/bankAnggota.html", $this->container);
	}

	public function saveByAjax(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->BankAnggotaModel->postAnggota($obj);
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));


        echo json_encode(array("type" => $result['type'], "msg" => $result['msg']));
	}

	public function uploadFile(){

		// Set path folder tempat menyimpan gambarnya
		$uploadDir = "./assets/imgRekening/";
		if(is_uploaded_file($_FILES['inputfile']['tmp_name'])){

			$uploadFile = $_FILES['inputfile'];
			// Extract nama file
			$extractFile = pathinfo($uploadFile['name']);
			$size = $_FILES['inputfile']['size']; //untuk mengetahui ukuran file
			$tipe = $_FILES['inputfile']['type'];// untuk mengetahui tipe file
			//Dibawah ini adalah untuk mengatur format gambar yang dapat di uplada ke server.
			//Anda bisa tambahakan jika ingin memasukan format yang lain tergantung kebutuhan anda.
			$exts =array('image/png','image/jpeg','image/jpg');
			if(!in_array(($tipe),$exts)){

				echo json_encode(array("type" => "error", "msg" =>"Sorry, you must be upload only file PDF and JPG", "name"=>""));

			}elseif(($size !=0)&&($size>(1000*1024))){

				// dibawah ini script untuk mengatur ukuran file yang dapat di upload ke server
				echo json_encode(array("type" => "error", "msg" =>"Sorry, file max. 1 MB", "name"=>""));
			}else{

				$sameName = 0; // Menyimpan banyaknya file dengan nama yang sama dengan file yg diupload
				$handle = opendir($uploadDir);
				while(false !== ($file = readdir($handle))){ // Looping isi file pada directory tujuan
					// Apabila ada file dengan awalan yg sama dengan nama file di uplaod
					if(strpos($file,$extractFile['filename']) !== false)
					$sameName++; // Tambah data file yang sama
				}
			 
				/* Apabila tidak ada file yang sama ($sameName masih '0') maka nama file pakai 
				* nama ketika diupload, jika $sameName > 0 maka pakai format "namafile($sameName).ext */
				$newName = empty($sameName) ? $uploadFile['name'] : $extractFile['filename'].'('.$sameName.').'.$extractFile['extension'];

					if(move_uploaded_file($uploadFile['tmp_name'],$uploadDir.$newName)){
						// echo json_encode(array("type" => "success", "msg" =>"Completed"));
						echo json_encode(array("type" => "success", "msg" =>"Gambar Sesuai", "name"=>$newName));
					}else{
						echo json_encode(array("type" => "error", "msg" =>"Error", "name"=>""));
					}
			}
			
		}
	}


}
