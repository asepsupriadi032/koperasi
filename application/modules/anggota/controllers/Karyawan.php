<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model("BankAnggotaModel");

		LoggedSystem();		
	}

	public function index($ang_id = null)
	{	
		$param['ang_id'] = $ang_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->BankAnggotaModel->getData($obj);
		$this->container['edit'] = $data['row'];
		// var_dump($this->container['edit']);
		$this->twig->display("form/bankAnggota.html", $this->container);
	}


}
