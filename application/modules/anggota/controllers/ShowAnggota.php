<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShowAnggota extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	 
		$this->load->helper('utility');	 
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model('AnggotaModel');
		
		//$this->container['dataReligion'] = $this->db->get("religion")->result();

		LoggedSystem();		
	}

	public function index($tag = NULL){

		$this->container['tag'] = $tag;
		
		$this->twig->display("grid/gridShowAnggota.html", $this->container);
	}
	
	public function getAjaxData($tag = NULL){
		// var_dump($tag); exit();
		$param['tag'] = $tag;
		$param['action'] = "showAnggota";
		$param['token'] = $this->tokenAPI->getToken();
		$obj= (object) $param;	
		// $data = json_decode($this->curl->simple_get($this->API.'/menu', $params));
		$data = $this->AnggotaModel->getAnggota($obj);
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				
				$responce->data[] = array(
					$row->ang_id, 
					$row->kat_kode, 
					$row->ang_nama,
					$row->kar_pekerjaan,
					$row->ang_alamat,
					angStatus($row->ang_status),//5
					$row->ang_hp,
					$row->ang_limit,
					$row->ang_dana,
					$row->bnk_nama,//9
					$row->bnk_pemilik,
					$row->bnk_norek,
					$row->kat_nama,//12
					$row->kat_kategori,//13
					$row->kat_bunga
				);
			}
		}		
		echo json_encode($responce);
	}


}