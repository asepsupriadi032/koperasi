<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShowUser extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	 
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model('UserModel');
		
		//$this->container['dataReligion'] = $this->db->get("religion")->result();

		LoggedSystem();		
	}

	public function index($tag = NULL){

		$this->container['tag'] = $tag;
		$this->twig->display("grid/gridShowUser.html", $this->container);
	}
	
	public function getAjaxData($tag = NULL){
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->UserModel->getUser($obj);

		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$responce->data[] = array(
					$row->usr_id,
					$row->usr_name,
					$row->ust_user_type,
					$row->usr_email,
					$row->usr_hp
				);
			}
		}		
		echo json_encode($responce);
	}


}