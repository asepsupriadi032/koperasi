<?php
class AnggotaBiodataModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        // $this->load->model('utility/LogModel');
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getData($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->select('ang_anggota.*, mnu_user.usr_name, kat_produk.kat_nama, kat_produk.kat_kode');
            $this->db->where('ang_anggota.ang_id',$obj->ang_id);
            $this->db->join("ang_karyawan","ang_karyawan.ang_id=ang_anggota.ang_id","left");
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $this->db->join('mnu_user','mnu_user.usr_id=ang_anggota.usr_menyetujui','left');
            $data = $this->db->get('ang_anggota')->row();

            $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postAnggota($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $angId = null;
        if($tokenValid){
                // $this->db->set("kat_id", $obj->kat_id);
                $this->db->set("ang_nama", $obj->ang_nama);
                $this->db->set("ang_ttl", $obj->ang_ttl);
                $this->db->set("ang_alamat", $obj->ang_alamat);
                $this->db->set("ang_ktp", $obj->ang_ktp);              
                $this->db->set("ang_kk", $obj->ang_kk);
                // $this->db->set("ang_pekerjaan", $obj->ang_pekerjaan);
                $this->db->set("ang_npwp", $obj->ang_npwp);
                $this->db->set("ang_hp", $this->cekHp($obj->ang_hp));
                $this->db->set("ang_email", $obj->ang_email);
                $this->db->set("ang_profil", $obj->ang_img_anggota);
                $this->db->set("ang_limit", $obj->ang_limit);
                $this->db->set("ang_dana", $obj->ang_dana);
                $this->db->set("usr_menyetujui", $obj->usr_menyetujui);

                if(!empty($obj->ang_foto_ktp)){

                    $this->db->set("ang_foto_ktp", $obj->ang_foto_ktp);
                }

                if(!empty($obj->ang_id)) {
                    $action = "Update";
                    $angId = $obj->ang_id;   
                    $this->db->set("ang_chby", $this->session->userdata("hcmUser"));
                    $this->db->set("ang_status", $obj->ang_status);
                    $this->db->set("ang_chdt", date('Y-m-d H:i:s'));
                    $this->db->where('ang_id', $obj->ang_id);
                    $exec = $this->db->update("ang_anggota");
                }
                else {
                    $action = "Insert";

                    $this->db->set("ang_status", "2");
                    $this->db->set("ang_crby", $this->session->userdata("hcmUser"));
                    $this->db->set("ang_crdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->insert('ang_anggota'); 
                    $angId = $this->db->insert_id();    
                }

                if($exec) {
                    //Save Log Db
                    $modul = "Anggota";
                    $usr_id = $this->session->userdata("hcmIdUser");
                    $valid = $this->addLog($action, $modul, "ang_anggota", $obj, $usr_id);
                    // End Save Log
                    $output=array("type" => "success", "msg" => "Request success, data saved!" , "angId"=>$angId);
    
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function addLog($act, $modul, $table, $record, $id_user = NULL){
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $this->session->userdata("hcmIdUser");
        }

        if ($act == "Delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("usr_id", $id_user);
        $this->db->set("log_action", $act);
        $this->db->set("log_date", date("Y-m-d H:i:s"));
        $this->db->set("log_record", $dataRecord);
        $this->db->set("log_table", $table);
        $this->db->set("log_modul", $modul);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }

    public function cekHp($nohp) {
        // kadang ada penulisan no hp 0811 239 345
        $nohp = str_replace(" ","",$nohp);
        // kadang ada penulisan no hp (0274) 778787
        $nohp = str_replace("(","",$nohp);
        // kadang ada penulisan no hp (0274) 778787
        $nohp = str_replace(")","",$nohp);
        // kadang ada penulisan no hp 0811.239.345
        $nohp = str_replace(".","",$nohp);
        // kadang ada penulisan no hp 0811-239-345
        $nohp = str_replace("-","",$nohp);

        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($nohp))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($nohp), 0, 3)=='+62'){
                $hp = trim($nohp);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($nohp), 0, 1)=='0'){
                $hp = '+62'.substr(trim($nohp), 1);
            }
        }
        return $hp;
    }
}