<?php
class AnggotaKaryawanModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');	
        // $this->load->model('utility/LogModel');	
		$this->load->helper('url');	
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getData($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->select('ang_karyawan.*,  ins_instansi.ins_nama, ins_sub_instansi.sin_nama, kat_produk.kat_nama, kat_produk.kat_kode, ins_sub_instansi.sin_kode');
            $this->db->where('ang_anggota.ang_id',$obj->ang_id);
            $this->db->join('ang_anggota','ang_anggota.ang_id=ang_karyawan.ang_id','left');
            $this->db->join('ins_instansi','ins_instansi.ins_id=ang_karyawan.ins_id','left');
            $this->db->join('ins_sub_instansi','ins_sub_instansi.sin_id=ang_karyawan.sin_id', 'left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $this->db->join('kat_karyawan','kat_karyawan.kar_kat_id=ang_karyawan.kar_kat_id','left');
            $data = $this->db->get('ang_karyawan')->row();

            $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function getKatInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $data = $this->db->get('ins_instansi')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function postAnggota($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

            $this->db->set("kar_kat_id",$obj->kar_kat_id);
            $this->db->set("ins_id",$obj->ins_id);
            $this->db->set("sin_id",$obj->sin_id);
            $this->db->set("ang_id",$obj->ang_id);
            $this->db->set("kat_id",$obj->kat_id);
            $this->db->set("kar_induk",$obj->kar_induk);
            $this->db->set("kar_pekerjaan",$obj->kar_pekerjaan);
            $this->db->set("kar_mulai_kerja",$obj->kar_mulai_kerja);
            $this->db->set("kar_status",$obj->kar_status);
            $this->db->set("kar_gaji",$obj->kar_gaji);

            if(!empty($obj->kar_id)){
                    $action = "Update";
     
                    $this->db->where('kar_id', $obj->kar_id);
                    $this->db->set("kar_chdt", date('Y-m-d H:i:s'));
                    $this->db->set("kar_chby", $this->session->userdata("hcmUser"));
                    $exec = $this->db->update("ang_karyawan");
                }
                else {
                    $action = "Insert";
                    $this->db->set("kar_crdt", date('Y-m-d H:i:s'));
                    $this->db->set("kar_crby", $this->session->userdata("hcmUser"));
                    $exec = $this->db->insert('ang_karyawan');     
                }

            if($exec) {
                    //Save Log Db
                    $modul = "Anggota Karyawan";
                    $usr_id = $this->session->userdata("hcmIdUser");
                    $valid = $this->addLog($action, $modul, "ang_karyawan", $obj, $usr_id);
                    // End Save Log
                    $output=array("type" => "success", "msg" => "Request success, data saved!");

                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }
        }else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;
    }

    public function getDataKategori($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            
            $data = $this->db->get('kat_karyawan')->result();

            $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

     public function addLog($act, $modul, $table, $record, $id_user = NULL){
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $this->session->userdata("hcmIdUser");
        }

        if ($act == "Delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("usr_id", $id_user);
        $this->db->set("log_action", $act);
        $this->db->set("log_date", date("Y-m-d H:i:s"));
        $this->db->set("log_record", $dataRecord);
        $this->db->set("log_table", $table);
        $this->db->set("log_modul", $modul);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }
}