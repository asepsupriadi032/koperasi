<?php
class AnggotaModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        // $this->load->model('utility/LogModel');
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getAnggota($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        // $tokenValid = true;

        if($tokenValid){

            // $id = $this->get('id');
            if(!empty($obj->kat_id)){
                $this->db->where('ang_karyawan.kat_id',$obj->kat_id);
            }
            $this->db->select("kat_produk.*,ang_anggota.ang_id, ang_anggota.ang_nama, ang_anggota.ang_alamat, ang_anggota.ang_id, ang_anggota.ang_status, ang_anggota.ang_hp, ang_anggota.ang_limit, ang_anggota.ang_dana, ang_bank.bnk_nama, ang_bank.bnk_pemilik, ang_bank.bnk_norek, ang_karyawan.kar_pekerjaan");
            if($obj->action == "showAnggota"){

            $this->db->where('ang_anggota.ang_status','1');
            }
            $this->db->order_by('ang_anggota.ang_nama','ASC');
            $this->db->group_by('ang_anggota.ang_id');
            $this->db->join("ang_karyawan","ang_karyawan.ang_id=ang_anggota.ang_id","left");
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $this->db->join('ang_bank','ang_bank.ang_id=ang_anggota.ang_id','left');
            $this->db->join('mnu_produk_role','mnu_produk_role.kat_id=ang_karyawan.kat_id','left');
            $data = $this->db->get('ang_anggota')->result();

        // var_dump($data); exit();

            if(!empty($obj->id)){
                $this->db->where('mnu_menu.mnu_id', $obj->id);
                $this->db->select("mnu_menu.*, parent.mnu_name as parent_name");
                $this->db->order_by('mnu_menu.mnu_parent','ASC');
                $this->db->join("mnu_menu as parent","mnu_menu.mnu_parent=parent.mnu_id","left");
                $data = $this->db->get('mnu_menu')->row();
            }
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postAnggota($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
                $this->db->set("kat_id", $obj->kat_id);
                $this->db->set("ang_nama", $obj->ang_nama);
                $this->db->set("ang_ttl", $obj->ang_ttl);
                $this->db->set("ang_alamat", $obj->ang_alamat);
                $this->db->set("ang_ktp", $obj->ang_ktp);              
                $this->db->set("ang_kk", $obj->ang_kk);
                // $this->db->set("ang_pekerjaan", $obj->ang_pekerjaan);
                $this->db->set("ang_bank", $obj->ang_bank);
                $this->db->set("ang_norek", $obj->ang_norek);
                $this->db->set("ang_npwp", $obj->ang_npwp);
                $this->db->set("ang_hp", $obj->ang_hp);
                $this->db->set("ang_email", $obj->ang_email);

                if(!empty($obj->ang_id)) {
                    $action = "Update";
     
                    $this->db->where('ang_id', $obj->ang_id);
                    $exec = $this->db->update("ang_anggota");
                }
                else {
                    $action = "Insert";
                    $this->db->set("ang_status", "2");
                    $this->db->set("ang_crdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->insert('ang_anggota');     
                }

                if($exec) {
                    //Save Log Db
                    $modul = "Anggota";
                    $usr_id = $this->session->userdata("hcmIdUser");
                    $valid = $this->addLog($action, $modul, "ang_anggota", $obj, $usr_id);
                    // End Save Log
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
    
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
        return $output;

    }


    public function getKatInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $data = $this->db->get('ins_instansi')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function getSubInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $this->db->where("ins_id",$obj->ins_id);
        $data = $this->db->get('ins_sub_instansi')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function getDetail($obj){
        $output=array();
        $token = $obj['token'];
        $tokenValid = $this->tokenAPI->checkToken($token);
        $this->db->where("ang_id",$obj['ang_id']);
        $this->db->select("ang_nama, ang_id");
        $data = $this->db->get('ang_anggota')->row();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function deleteImgProfil($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

            
        if ($tokenValid) {
            $this->db->where("ang_id",$obj->ang_id);
            $this->db->select("ang_profil");
            $getData = $this->db->get('ang_anggota')->row();

            $img = $getData->ang_profil;
            $Dir = "./assets/imgAnggota/";
            $deleteImg = unlink($Dir.$img);

            if($deleteImg){
                $this->db->where('ang_id',$obj->ang_id);
                $this->db->set('ang_profil','');
                $this->db->update('ang_anggota');  
                
                $action = "delete";
                //save log
                $modul = "Image Anggota";
                $usr_id = $this->session->userdata("hcmIdUser");
                $valid = $this->addLog($action, $modul, "ang_anggota", array("ang_id" => $obj->ang_id), $usr_id);
                //and save log
                $output=array("type" => "success", "msg" => "Image deleted, data saved!");  
            }else{
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }
            


        }else{
            $output=array("type" => "error", "msg" => "Request failed, invalid token!");
        }
                      
        return $output;
    }

    public function deleteAnggota($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $action = "Delete";
            //save log
            $modul = "Anggota";
            $usr_id = $this->session->userdata("hcmIdUser");
            $valid = $this->addLog($action, $modul, "ang_anggota", array("ang_id" => $obj->ang_id), $usr_id);
            // $valid = $this->LogModel->addLog($action, $modul, "ang_anggota", array("ang_id" => $obj->ang_id), $usr_id);
            //and save log

            $this->db->where('ang_anggota.ang_id',$obj->ang_id);
            $exec = $this->db->delete('ang_anggota');

            if($exec){
                $output=array("type" => "success", "msg" => "Request success, data deleted!");
            }else{
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }else{
            $output=array("type" => "error", "msg" => "Request failed, invalid token!");
        }
        return $output;
    }

    public function delAnggota($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $action = "Delete";
            //save log
            $modul = "Anggota";
            $usr_id = $this->session->userdata("hcmIdUser");
            $valid = $this->addLog($action, $modul, "ang_anggota", array("ang_id" => $obj->ang_id), $usr_id);
            // $valid = $this->LogModel->addLog($action, $modul, "ang_anggota", array("ang_id" => $obj->ang_id), $usr_id);
            //and save log

            $this->db->where('ang_anggota.ang_id',$obj->ang_id);
            $exec = $this->db->delete('ang_anggota');

            if($exec){
                $output=array("type" => "success", "msg" => "Request success, data deleted!");
            }else{
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }else{
            $output=array("type" => "error", "msg" => "Request failed, invalid token!");
        }
        return $output;
    }

    public function editAnggota($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
                $this->db->set("ang_status", $obj->ang_status);

                if(!empty($obj->ang_id)) {
                    $action = "Update";  
                    $this->db->set("ang_chby", $this->session->userdata("hcmUser"));
                    $this->db->set("ang_status", $obj->ang_status);
                    $this->db->set("ang_chdt", date('Y-m-d H:i:s'));
                    $this->db->where('ang_id', $obj->ang_id);
                    $exec = $this->db->update("ang_anggota");
                }else {
                    $action = "Insert";
                    $this->db->set("ang_crby", $this->session->userdata("hcmUser"));
                    $this->db->set("ang_status", "2");
                    $this->db->set("ang_crdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->insert('ang_anggota');     
                }

                if($exec) {
                    //save log
                    $modul = "Anggota";
                    $usr_id = $this->session->userdata("hcmIdUser");
                    $valid = $this->addLog($action, $modul, "ang_anggota", $obj, $usr_id);
                    //and save log
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
    
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
        return $output;
    }

    public function addLog($act, $modul, $table, $record, $id_user = NULL){
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $this->session->userdata("hcmIdUser");
        }

        if ($act == "Delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("usr_id", $id_user);
        $this->db->set("log_action", $act);
        $this->db->set("log_date", date("Y-m-d H:i:s"));
        $this->db->set("log_record", $dataRecord);
        $this->db->set("log_table", $table);
        $this->db->set("log_modul", $modul);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }

}