<?php
class BankAnggotaModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        // $this->load->model('utility/LogModel');
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getData($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            // $this->db->select("ang_bank, ang_id, ang_norek");
            $this->db->where('ang_bank.ang_id',$obj->ang_id);
            $data = $this->db->get('ang_bank')->row();

            $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function getBank($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $data = $this->db->get('mst_bank')->result();

            $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postAnggota($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

            $this->db->set("ang_id",$obj->ang_id);
            $this->db->set("bnk_nama",$obj->bnk_nama);
            $this->db->set("bnk_pemilik",$obj->bnk_pemilik);
            $this->db->set("bnk_norek",$obj->bnk_norek);
            $this->db->set("bnk_img",$obj->bnk_img);

            if(!empty($obj->bnk_id)){
                    $action = "Update";
                    

                    $this->db->set("bnk_chby", $this->session->userdata("hcmUser"));
                    $this->db->set("bnk_chdt", date('Y-m-d H:i:s'));
                    $this->db->where('bnk_id', $obj->bnk_id);
                    $exec = $this->db->update("ang_bank");
                }
                else {
                    $action = "Insert";
                    $this->db->set("bnk_crby", $this->session->userdata("hcmUser"));
                    $this->db->set("bnk_crdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->insert('ang_bank');     
                }

            if($exec) {
                 //Save Log Db
                $modul = "Bank Anggota";
                $usr_id = $this->session->userdata("hcmIdUser");
                $valid = $this->addLog($action, $modul, "ang_bank", $obj, $usr_id);
                // End Save Log
                    $output=array("type" => "success", "msg" => "Request success, data saved!");

                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }
        }else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;
    }

     public function addLog($act, $modul, $table, $record, $id_user = NULL){
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $this->session->userdata("hcmIdUser");
        }

        if ($act == "Delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("usr_id", $id_user);
        $this->db->set("log_action", $act);
        $this->db->set("log_date", date("Y-m-d H:i:s"));
        $this->db->set("log_record", $dataRecord);
        $this->db->set("log_table", $table);
        $this->db->set("log_modul", $modul);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }
}