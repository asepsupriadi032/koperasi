<?php
class UserModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getUser($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->join('mnu_user_type','mnu_user_type.ust_id=mnu_user.ust_id','left');
            $data =  $this->db->get('mnu_user')->result();
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }


}