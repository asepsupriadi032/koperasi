<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	 
		$this->load->helper('utility');	 
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model('PembayaranModel');
		
		//$this->container['dataReligion'] = $this->db->get("religion")->result();

		LoggedSystem();		
	}

	public function index($tag = NULL){

		$this->container['tag'] = $tag;
		$this->twig->display("grid/gridPembayaran.html", $this->container);
	}
	
	public function getAjaxData($tag = NULL){
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->PembayaranModel->getData($obj);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				if($row->bun_kategori=="0"){
					$kategori = "Bulan";
				}else{
					$kategori= "Hari";
				}
				$responce->data[] = array(
					$x, 
					$row->tran_transaksi_id, 
					$row->kat_kode, 
					"<a href='".base_url('keuangan/Pembayaran/detailPembayaran/').$row->tran_id."'>".$row->ang_nama."</a>",
					date_format(date_create($row->tran_tgl_pengajuan), "m/d/Y"),
					date_format(date_create($row->tran_jatuh_tempo), "m/d/Y"),
					number_format($row->tran_nominal,0,",","."),					
					$row->tran_bulan." ".$kategori,
					$row->tran_bunga." %", 
					number_format($row->tran_perbulan,0,",","."),		 
					number_format($row->tran_total,0,",","."),		 
					number_format($row->tran_sisa,0,",","."),		 
					$row->tran_id//11
				);
			}
		}		
		echo json_encode($responce);
	}

	public function addPembayaran($tag = NULL, $angs_id=NULL){
		$param['angs_id'] = $angs_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->PembayaranModel->getPembayaran($obj);
		// var_dump($obj); exit();
		$this->container['row'] = $data['row'];
		$this->container['tgl'] = date ('dmY');
		$this->twig->display("form/formPembayaran.html", $this->container);
	}

	public function saveByAjax(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->PembayaranModel->postBayar($obj);
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
		echo json_encode(array("type" => $result['type'], "msg" => $result['msg']));
	}

	public function detailPembayaran($tran_id = NULL){
		$this->container['tran_id'] = $tran_id;

		$param['tran_id'] = $tran_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param; 
		$getIdTransaksi = $this->PembayaranModel->getId($obj);
		$this->container['idTransaksi'] = $getIdTransaksi['row'];
		$this->twig->display("grid/gridDetailPembayaran.html", $this->container);
	}

	public function getAjaxDataPembayaran($tran_id = NULL){
		$param['token'] = $this->tokenAPI->getToken();	
		$param['tran_id'] = $tran_id;
		$obj = (object) $param;
		$data = $this->PembayaranModel->getDataPembayaran($obj);
		// var_dump($data['row']->tran_bulan);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{
			foreach($data['row'] as $row) { 
				$x++;
				if($row->angs_status=="0"){
					$status = "Belum lunas";
				}elseif($row->angs_status=="1"){
					$status = "Lunas";
				}else{
					$status = "-";
				}
				$responce->data[] = array(
					$x, 
					$row->tran_transaksi_id, 
					$row->kat_kode,
					number_format($row->tran_perbulan,0,",","."), 
					date('d F Y', strtotime('+'.$x.'months', strtotime($row->tran_jatuh_tempo))),
					number_format($row->angs_kurang_bayar,0,",","."), 
					$status, 
					$row->angs_komentar, //7
					$row->angs_id,//8	
					$row->angs_status//9			
				);
			}			
		}		
		echo json_encode($responce);
	}

	public function statusPembayaran($tanggal, $tran_id){

		$data = $this->PembayaranModel->cekPembayaran($tanggal, $tran_id);
		return $data;
	}

}