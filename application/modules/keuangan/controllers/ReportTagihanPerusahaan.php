<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportTagihanPerusahaan extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('pdf');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();

		$this->load->model("ReportTagihanPerusahaanModel");		

		LoggedSystem();		
	}

	public function displayreport($kat_id = NULL){			
		$param['token'] = $this->tokenAPI->getToken();	
		$param['kat_id'] = $kat_id;
		$obj = (object) $param;
		$data = $this->ReportTagihanPerusahaanModel->getData($obj);
		// var_dump($data);exit();

		$this->container['kat_kode']=$data['produk']->kat_kode;
		$this->container['tanggalTagihan'] = date('d M Y');
		$this->container['totalTagihan'] =number_format($data['totalTagihan'],2,',','.');
		$this->container['kat_id'] = $kat_id;
		$this->container['row'] = $data['row'];
		$this->twig->display("grid/gridReportTagihanPerusahaan.html", $this->container);
	}

	public function cetakPdf(){
	// public function cetakPdf($kat_id,$inv_penerima,$inv_tagihan,$inv_jatuh_tempo,$inv_bank,$inv_pemilike,$inv_norek){
    
    //execute
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		// var_dump($obj); exit();
	    $this->ReportTagihanPerusahaanModel->cetakPdf($obj);
	    // $this->ReportTagihanPerusahaanModel->cetakPdf($kat_id,$inv_penerima,$inv_tagihan,$inv_jatuh_tempo,$inv_bank,$inv_pemilike,$inv_norek);
	  }

  public function detailCetakPdf($kat_id = NULL){

  	$param['token'] = $this->tokenAPI->getToken();
	$obj = (object) $param;
  	$data2 = $this->ReportTagihanPerusahaanModel->getBank($obj);
	$this->container['bank'] = $data2['row'];

  	$this->container['kat_id'] = $kat_id;
  	$this->twig->display("grid/gridDetailReportTagihanPerusahaan.html", $this->container);
  }

  public function getAjaxData($kat_id = NULL){
		if(!empty($kat_id)){
			$param['kat_id'] = $kat_id;
		}
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->ReportTagihanPerusahaanModel->getData($kat_id);
		// var_dump($kat_id); exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$kode = "";
				$nama = "";
				
				if($row->tran_status_operational !="3"){
                    $status = "-";
                }else{
                    $status = statusTransaksi($row->tran_status);
                }
				$responce->data[] = array(
					$x, 
					$row->tran_transaksi_id, 
					$row->tran_jatuh_tempo,
					$row->ang_nama,
					$row->kat_kode,
					number_format(ceil($row->tran_perbulan),2,',','.'),
					$row->tran_bunga." %",
					$status,
				);
			}
		}		
		echo json_encode($responce);
	}	

	

}
