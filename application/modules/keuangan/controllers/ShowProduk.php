<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShowProduk extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	 
		$this->load->helper('utility');	 
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();	
		$this->load->model('PembayaranModel');
		
		//$this->container['dataReligion'] = $this->db->get("religion")->result();

		LoggedSystem();		
	}

	public function index($tag = NULL){

		$this->container['tag'] = $tag;
		$this->twig->display("grid/gridShowProduk.html", $this->container);
	}
	
	public function getAjaxData($tag = NULL){
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->PembayaranModel->getDataProduk($obj);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$responce->data[] = array(
					$x, 
					$row->ins_nama,
					$row->sin_nama,
					$row->sin_alamat,
					$row->kat_kode,
					katProduk($row->kat_kategori),//5
					$row->kat_id,
					$row->kat_nama 
				);
			}
		}		
		echo json_encode($responce);
	}



}