<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TagihanPerusahaan extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;	
		$this->tokenAPI = new TokenAPI();

		$this->load->model("TagihanPerusahaanModel");		

		LoggedSystem();		
	}

	public function index($ang_id = NULL)
	{			
		$this->container['ang_id'] = $ang_id;
		$this->twig->display("grid/gridTagihanPerusahaan.html", $this->container);
	}

	

}
