<?php
class PembayaranModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getData($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->where('tran_transaksi.tran_status_operational','3');
            $this->db->select('tran_transaksi.*, ang_anggota.ang_nama, kat_produk.kat_kode, kat_produk.bun_kategori');
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $data =  $this->db->get('tran_transaksi')->result();
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $output = array("type" => "error", "msg" => "Invalid token!");
        }
        return $output;
    }

    public function getPembayaran($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->select('tran_transaksi.*, ang_anggota.ang_nama, kat_produk.kat_kode, angs_angsuran.angs_id, angs_angsuran.angs_kurang_bayar, angs_angsuran.angs_nominal');
            $this->db->where('angs_angsuran.angs_id',$obj->angs_id);
            $this->db->join('tran_transaksi','tran_transaksi.tran_id=angs_angsuran.tran_id');
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $data = $this->db->get('angs_angsuran')->row();
            $output=array("type" => "success", "row" => $data);
        }else{
            $output = array("type"=>"error", "msg"=>"Invalid token!");
        }

        return $output;
    }

    public function postBayar($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $kurang_bayar = 0;
            $angs_status = 1;
            $angs_nominal =$obj->angs_nominal;
            
            if(!empty($obj->angs_kurang_bayar)){
                if($obj->angs_nominal < $obj->angs_kurang_bayar){
                    $angs_status = 0;
                    $kurang_bayar = $obj->angs_kurang_bayar - $obj->angs_nominal;
                    $angs_nominal =  $obj->angs_kurang_bayar + $obj->nominal;
                }else{
                    $angs_status = 1;
                    $kurang_bayar = 0;
                    $angs_nominal =  $obj->angs_kurang_bayar + $obj->nominal;
                }
            }else{
                if($obj->angs_nominal < $obj->tran_perbulan){
                    $kurang_bayar = $obj->perbulan - $obj->angs_nominal;
                    $angs_status = 0;
                }
            }
 
            $this->db->set('angs_tgl',$obj->angs_tgl);
            $this->db->set('angs_nominal',$angs_nominal);
            $this->db->set('angs_status', $angs_status);
            $this->db->set('angs_komentar', $obj->angs_komentar);
            $this->db->set('angs_kurang_bayar',$kurang_bayar);
            $this->db->set('tran_id',$obj->tran_id);
            $this->db->where('angs_id',$obj->angs_id);
            $this->db->set("angs_chdt", date('Y-m-d H:i:s'));
            $this->db->set("angs_chby", $this->session->userdata("hcmUser"));
            $exec = $this->db->update("angs_angsuran");

            // var_dump($exec); exit();
            if($exec) {

                    $this->db->where('tran_transaksi.tran_id',$obj->tran_id);
                    $tran_sisa = $obj->sisa - $obj->angs_nominal;
                    // var_dump($tran_sisa); exit();
                    $this->db->set('tran_sisa', $tran_sisa);
                    $this->db->update('tran_transaksi');
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
                }else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }
        }else{
            $output = array("type"=>"error", "msg"=>"Invalid token!");
        }

        return $output;
    }

    public function getDataProduk($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->select("kat_produk.*, ins_instansi.ins_nama, ins_sub_instansi.sin_nama, ins_sub_instansi.sin_alamat");
            $this->db->join('ins_sub_instansi','ins_sub_instansi.sin_id=kat_produk.sin_id','left');
            $this->db->join('ins_instansi','ins_instansi.ins_id=ins_sub_instansi.ins_id','left');
            $data = $this->db->get('kat_produk')->result();
            $output=array("type" => "success", "row" => $data);
        }else{
            $output = array("type"=>"error", "msg"=>"Invalid token!");
        }

        return $output;
    }

    public function getId($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->select('tran_transaksi.tran_transaksi_id, ang_anggota.ang_nama, kat_produk.kat_kode');
            $this->db->where('tran_transaksi.tran_id',$obj->tran_id);
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id');
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=tran_transaksi.ang_id');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id');
            $data = $this->db->get('tran_transaksi')->row();
            $output=array("type" => "success", "row" => $data);
        }else{
            $output = array("type"=>"error", "msg"=>"Invalid token!");
        }

        return $output;
    }

    public function getDataPembayaran($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->select('angs_angsuran.*, tran_transaksi.tran_transaksi_id, tran_transaksi.tran_jatuh_tempo, kat_produk.kat_kode, tran_transaksi.tran_perbulan, tran_transaksi.tran_jatuh_tempo');
            $this->db->order_by('angs_angsuran.angs_id','ASC');
            $this->db->where('angs_angsuran.tran_id',$obj->tran_id);
            $this->db->join('tran_transaksi','tran_transaksi.tran_id=angs_angsuran.tran_id');
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=tran_transaksi.ang_id');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id');
            $data = $this->db->get('angs_angsuran')->result();
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $output = array("type" => "error", "msg" => "Invalid token!");
        }
        return $output;
    }

    // public function cekPembayaran($tanggal, $tran_id){
    //     $this->db->where('angs_angsuran.')
    // }


}