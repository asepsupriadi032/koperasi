<?php
class ReportTagihanPerusahaanModel extends CI_Model {
    private $container;
    private $valid = false;
    private $API;
    private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
        $this->load->helper('token');       
        $this->load->helper('url'); 
        $this->load->helper('utility'); 
        $this->load->helper('pdf');
        $this->container['data'] = null;
        $this->output = array();

        
        // $this->API = $this->config->item('api_url')."/utility";  
        $this->tokenAPI = new TokenAPI();
    }

    public function getBank($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $data = $this->db->get('mst_bank')->result();

            $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function getData($kat_id){
        $output=array();
        // $token = $obj->token;
        // $tokenValid = $this->tokenAPI->checkToken($token);

        // if($tokenValid){

            $this->db->select('tran_transaksi.*, ang_anggota.ang_nama,kat_produk.kat_kode, tran_transaksi.tran_status');
            $this->db->where('ang_karyawan.kat_id',$kat_id);
            $this->db->where('tran_transaksi.tran_status_operational','3');
            $this->db->join('ang_anggota','ang_anggota.ang_id=ang_karyawan.ang_id');
            $this->db->join('tran_transaksi','tran_transaksi.ang_id=ang_karyawan.ang_id');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id');
            // $this->db->join('mst_bunga','mst_bunga.bun_id=tran_transaksi.bun_id');
            $data = $this->db->get('ang_karyawan')->result();

            // var_dump($data); exit();
            $totalTagihan = 0;
            foreach ($data as $row) {
                $totalTagihan = $totalTagihan + ceil($row->tran_perbulan); 
            }

            $this->db->select('kat_produk.kat_kode, kat_produk.kat_nama, ins_sub_instansi.sin_nama, ins_sub_instansi.sin_alamat');
            $this->db->where('kat_produk.kat_id', $kat_id);
            $this->db->join('ins_sub_instansi','ins_sub_instansi.sin_id=kat_produk.sin_id','left');
            $produk = $this->db->get('kat_produk')->row();

            $output=array("type" => "success", "row" => $data, "totalTagihan"=>$totalTagihan, "produk"=>$produk);

        // }
        // else{
        //     $this->response(array("type" => "error", "msg" => "Invalid token!"));
        // }
        return $output;
    }

    public function ubahTanggal($date){
        $hasil="";
        $bln= "";
        $tahun= substr($date,0,4);
        $bulan= substr($date,5,2);
        $tgl= substr($date,8,2);

        switch ($bulan) {
            case '01':
                $bln = "Januari";
            break;
            case '01':
                $bln = "Januari";
            break;
            case '02':
                $bln = "Februari";
            break;
            case '03':
                $bln = "Maret";
            break;
            case '04':
                $bln = "April";
            break;
            case '05':
                $bln = "Mei";
            break;
            case '06':
                $bln = "Juni";
            break;
            case '07':
                $bln = "Juli";
            break;
            case '08':
                $bln = "Agustus";
            break;
            case '09':
                $bln = "September";
            break;
            case '10':
                $bln = "Oktober";
            break;
            case '11':
                $bln = "November";
            break;
            case '12':
                $bln = "Desember";
            break;
            
            default:
                $bln = $bulan;
            break;
        }

        $hasil = $tgl." ".$bln." ".$tahun;

        return $hasil;
    }

    public function cetakPdf($obj){
        // var_dump($obj); exit();
        // $penerima = str_replace('%20', ' ', $obj->inv_penerima);
        $penerima = "";
        $pemilik = str_replace('%20', ' ', $obj->inv_pemilik);
        $data = $this->getData($obj->kat_id);
        $produk = $data['produk']->kat_kode;
        $totalTagihan = $data['totalTagihan'];
        $pembayaran = 0;
        $sisaTagihan = $totalTagihan-$pembayaran;
        $tglTagihan = $this->ubahTanggal($obj->inv_tagihan);
        $tglJatuhTempo = $this->ubahTanggal($obj->inv_jatuh_tempo);

        $judul = 'Invoice '.$produk.' '.$tglTagihan;
        $kolom = array('keterangan'=>'Keterangan',
                      'jumlah' => 'Jumlah',
                      'tanggal' => 'Tanggal'
                );
      //query
        // $data = $this->db->query("select ang_nama,ang_hp,ang_alamat from ang_anggota 
        //        where ang_id='1'")->result();          
      // $data = $query->result_array();
        $data2 = $data['row'];
        $produk2 = $data['produk'];
        
        // var_dump($tglPembuatan); exit();
        $this->generatePdf2($data2,$judul, $produk2, $penerima,$tglTagihan,$tglJatuhTempo,$obj->inv_bank,$pemilik,$obj->inv_norek, $totalTagihan, $pembayaran, $sisaTagihan);  
    }

    private function generatepdf2($data,$judul,$produk2,$penerima,$tglTagihan,$tglJatuhTempo,$inv_bank,$pemilik,$inv_norek, $totalTagihan, $pembayaran, $sisaTagihan){
        // var_dump($totalTagihan); exit();
        $produk = $produk2->kat_kode;
        $instansi = $produk2->sin_nama;
        $instansiAlamat = $produk2->sin_alamat;
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // $pdf->SetTitle('Daftar Produk');
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(40);
        $pdf->setFooterMargin(25);
        // $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Author');
        //tanpa header dan footer
            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(true);
        //batas tanpa header dan footer
        // set default header data
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $pdf->SetTitle($judul);
        //header

        // Kopsurat
        // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        // $pdf->Image(base_url("assets/pages/img/login/kopHeader.png"), 15, 10, 25, 23, 'PNG', '', 'left', true, 150, '', false, false, 0, false, false, false);
        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        // $txt = "KOPERASI TASS INDONESIA NUSANTARA";
        $pdf->MultiCell(100, 5, '', 0, 'C', 0, 0, 60, 10, true);
        $pdf->ln(0);
        //Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='', $stretch=0, $ignore_min_height=false, $calign='T', $valign='M')
        $pdf->SetFont('helvetica', 'B', 16);
        $pdf->Cell(0, 0, 'KOPERASI TASS INDONESIA NUSANTARA', 0, 1, 'C', 0, '', 0);

        $pdf->SetFont('helvetica', '', 12);
        $pdf->Cell(0, 0, 'Jl. Ridwan Rais No. 18-22 Beji Timur Depok (16422)', 0, 1, 'C', 0, '', 0);
        $pdf->Cell(0, 0, 'Telp. 021-77201178 Fax. 021-77203599', 0, 1, 'C', 0, '', 1);
        $pdf->Cell(0, 0, 'www.koptassindo.com', 0, 1, 'C', 0, '', 1);
        // $pdf->Cell(0, -10, '<H3>KOPERASI TASS INDONESIA NUSANTARA</h3>', 0, false, 'C', 0, '', 0, false, 'T', 'M'); 
        // $pdf->writeHTML("<H3>KOPERASI TASS INDONESIA NUSANTARA", true, false, true, false, '');
        $pdf->ln(3);
        $pdf->writeHTML("<hr>", true, false, true, false, '');
        $pdf->SetFont('helvetica', '', 10);
        //batas Kop surat

//header
$tabel = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
    table.first tr td {
        font-family: helvetica;
        font-size: 10pt;

    }
    td {
      
        margin:10px;
        padding : 20px;
    }
    td.second {
       
    }
    .lowercase {
        text-transform: lowercase;
    }
    .uppercase {
        text-transform: uppercase;
    }
    .capitalize {
        text-transform: capitalize;
    }
</style>
<table class="first" width="100%" cellpadding="2" cellspacing="2">
 <tr>
  <td align="left" colspan="3"><b>Invoice#$produk</b></td>
 </tr>
 <tr>
  <td align="left" width="270">$instansi</td>
  <td align="Left" width="120">Id Produk</td>
  <td align="Left">: $produk</td>
 </tr>
 <tr>
  <td align="left" width="270" class="capitalize">$instansiAlamat</td>
  <td align="Left" width="120">Tanggal Tagihan</td>
  <td align="Left">: $tglTagihan</td>
 </tr>
 <tr>
  <td align="left" width="270" class="capitalize">Fax - </td>
  <td align="Left" width="120">Tanggal Jatuh Tempo</td>
  <td align="Left">: $tglJatuhTempo</td>
 </tr>
</table>
EOF;

// output the HTML content
$pdf->writeHTML($tabel, true, false, true, false, '');
//batas Header

    $pdf->ln(20);
    $pdf->SetFont('helvetica', '', 10);
    $html='<table cellspacing="1" class="first" bgcolor="#666666" cellpadding="2">
                <tr>
                    <th width="35%" align="center"><b>Total Tagihan</b></th>
                    <th width="35%" align="center"><b>Total Pembayaran</b></th>
                    <th width="30%" align="center"><b>Sisa</b></th>
                    </tr>';
        $html.='<tr bgcolor="#ffffff">
                    <td align="center"> Rp. '.number_format($totalTagihan,2,',','.').'</td>
                    <td align="center">Rp. '.number_format($pembayaran,2,',','.').'</td>
                    <td align="center">Rp. '.number_format($sisaTagihan,2,',','.').'</td>
                </tr>';
    $html.='</table>';
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->ln(10);
    $pdf->SetFont('helvetica', 'B', 10);
    $pdf->Cell(0, 0, 'Informasi Rekening Tujuan', 0, 1, 'L', 0, '', 0);

    $pdf->ln(5);
    $pdf->SetFont('helvetica', '', 10);
    $html='<table cellspacing="1" class="first" bgcolor="#666666" cellpadding="2">
                <tr>
                    <th width="35%" align="center"><b>Nama Bank</b></th>
                    <th width="35%" align="center"><b>Nama Pemilik Rekening</b></th>
                    <th width="30%" align="center"><b>Nomor Rekening</b></th>
                    </tr>';
        $html.='<tr bgcolor="#ffffff">
                    <td align="center">'.$inv_bank.'</td>
                    <td align="center">'.$pemilik.'</td>
                    <td align="center">'.$inv_norek.'</td>
                </tr>';
    $html.='</table>';
    $pdf->writeHTML($html, true, false, true, false, '');

    $pdf->ln(15);
    $pdf->Cell(0, 0, 'Catatan:', 0, 1, 'L', 0, '', 0);
    $pdf->Cell(0, 30, '', 1, 1, 'L', 0, '', 0);
    $pdf->ln(20);
    $pdf->Cell(0, 0, 'Syarat dan ketentuan:', 0, 1, 'L', 0, '', 0);
    $html="<ul>
            <li>Pinalti akan dikenakan pada pembayaran yang telah jatuh tempo.</li>
            <li>Pembayaran harus dilakukan melalui transfer bank atau setor tunai ke rekening bank yang ditunjuk.</li>
            <li>Mohon kirim konfirmasi pembayaran ke koptassindo@gmail.com</li>
        </ul>";
     $pdf->writeHTML($html, true, false, true, false, '');
    // $pdf->MultiCell(100, 5, 'sdfs', 1, 'C', 0, 0, 60, 10, true);
        //halaman Baru
        $pdf->AddPage();
        // // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
        // $pdf->Image(base_url("assets/pages/img/login/kopHeader.png"), 15, 10, 25, 23, 'PNG', '', 'left', true, 150, '', false, false, 0, false, false, false);
        // $pdf->ln(20);
        $pdf->SetFont('helvetica', '', 9);
        $i=0;
        $html='<table cellspacing="1" width="100%" bgcolor="#666666" cellpadding="2">
                    <tr bgcolor="#ffffff">
                        <th width="5%" align="center"style="vertical-align:middle"><b>No</b></th>
                        <th width="13%" align="center"><b>Id Transaksi</b></th>
                        <th width="12%" align="center"><b>Tanggal Tagihan</b></th>
                        <th width="25%" align="center"><b>Nama</b></th>
                        <th width="12%" align="center"><b>Produk</b></th>
                        <th width="15%" align="center"><b>Tagihan</b></th>
                        <th width="9%" align="center"><b>Admin App & Provisi</b></th>
                        <th width="9%" align="center"><b>Status</b></th>
                    </tr>';

        foreach ($data as $row) 
            {
                $i++;
                $tagihan = ceil($row->tran_perbulan);
                if($row->tran_status_operational !="3"){
                    $status = "-";
                }else{
                    $status = statusTransaksi($row->tran_status);
                }
                $html.='<tr bgcolor="#ffffff">
                        <td align="center">'.$i.'</td>
                        <td align="center">'.$row->tran_transaksi_id.'</td>
                        <td align="center">'.$row->tran_jatuh_tempo.'</td>
                        <td align="center">'.$row->ang_nama.'</td>
                        <td align="center">'.$row->kat_kode.'</td>
                        <td align="center">'.number_format($tagihan,2,',','.').'</td>
                        <td align="center">'.$row->tran_bunga.' %</td>
                        <td align="center">'.$status.'</td>
                    </tr>';
            }
        $html.='</table>';
        $pdf->writeHTML($html, true, false, true, false, '');
        // $no="";
        // for ($j=0; $j < 100; $j++) { 
        //     $no .= $j."<br>";
        // }

        // $pdf->writeHTML($no, true, false, true, false, '');
        // $pdf->SetY(-40); 
        // $pdf->writeHTML("batas", true, true, true, true, ''); 
        //  $pdf->SetY(-15); 
        // $pdf->writeHTML("<hr>", true, false, false, false, ''); 

         // $pdf->SetFont('helvetica', '', 12); 
         // $pdf->Cell(0, 10, ''.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M'); 
        $pdf->Output($judul.'.pdf', 'I');
  }

}