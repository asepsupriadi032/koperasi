<?php
class TagihanPerusahaanModel extends CI_Model {
    private $container;
    private $valid = false;
    private $API;
    private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
        $this->load->helper('token');       
        $this->load->helper('url'); 
        $this->container['data'] = null;
        $this->output = array();

        
        // $this->API = $this->config->item('api_url')."/utility";  
        $this->tokenAPI = new TokenAPI();
    }

    public function getAnggotaTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            // $this->db->where('tran_transaksi.ang_id',$obj->ang_id);
            $this->db->select('tran_transaksi.*, kat_produk.kat_kode, ang_anggota.ang_dana, ang_anggota.ang_limit, mst_bunga.bun_bulan, mst_bunga.bun_persen');
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=ang_anggota.ang_id','left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $this->db->join('mst_bunga','mst_bunga.bun_id=tran_transaksi.bun_id','left');
            $data = $this->db->get('tran_transaksi')->result();
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

}