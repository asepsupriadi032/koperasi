<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		
		// $this->API = $this->config->item('api_url')."/utility";
		$this->load->model('LogAktivitasModel');
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridLog.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		// $data = json_decode($this->curl->simple_get($this->API.'/menu', $params));
		$data = $this->LogAktivitasModel->getDataLog($obj);
		// var_dump($data); exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				
					
				$responce->data[] = array(
					$x, 
					$row->usr_name, 
					$row->log_date,
					$row->log_action,
					$row->log_modul,
					$row->log_record
				);
			}
		}		
		echo json_encode($responce);
	}	

}
