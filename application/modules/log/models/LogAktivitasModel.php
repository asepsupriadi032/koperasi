<?php
class LogAktivitasModel extends CI_Model {
    private $container;
    private $valid = false;
    private $API;
    private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
        $this->load->helper('token');       
        $this->load->helper('url'); 
        // $this->load->model('./produk/SetProdukModel');
        $this->container['data'] = null;
        $this->output = array();    
        $this->tokenAPI = new TokenAPI();
    }

    public function getDataLog($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->order_by('mnu_log.log_id','DESC');
            $this->db->join('mnu_user','mnu_user.usr_id=mnu_log.usr_id','left');
            $data = $this->db->get('mnu_log')->result();

            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }


}