<?php
class LogAnggotaModel extends CI_Model {
    private $container;
    private $valid = false;
    private $API;
    private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
        $this->load->helper('token');       
        $this->load->helper('url'); 
        // $this->load->model('./produk/SetProdukModel');
        $this->container['data'] = null;
        $this->output = array();    
        $this->tokenAPI = new TokenAPI();
    }

    public function getDataLog($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $this->db->order_by('ang_log.ang_log_id','DESC');
            $this->db->join('ang_anggota','ang_anggota.ang_id=ang_log.ang_id','left');
            $data = $this->db->get('ang_log')->result();

            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }


}