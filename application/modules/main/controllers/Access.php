<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Access extends REST_Controller {

    private $tokenAPI;

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('modelAccess');
        $this->load->helper('token'); 
        $this->tokenAPI = new TokenAPI();   

    }

 
    function index_post() {

        $obj = (object) $this->post();
        $tokenValid = $this->tokenAPI->checkToken($obj->token);
        
        $result = array("type" => "error", "msg" => "Invalid token!");
        print_r($obj);exit();
        
        if($tokenValid){
            
            $result = $this->modelAccess->action($obj);
            // print_r($result);die;

            $this->response($result);
        }

        $this->response($result);

    }


    function index_get() {

        $obj = (object) $this->get();
        $tokenValid = $this->tokenAPI->checkToken($obj->token);

        $result = array("type" => "error", "msg" => "Invalid token!");

        if($tokenValid){
           /* Lempar ke Action Method aja */
           switch ($this->get("action")){
				
				default:
				/* Lempar ke Action Method aja */
				$result = $this->modelAccess->action($obj);
				break;
		   }
           /* End Lempar ke Action, output nya Array session Flash Data*/

           $this->response($result);
        }

        $this->response($result);
    }

    
}
?>