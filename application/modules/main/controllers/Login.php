<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	private $container;
	private $valid = false;

	public function __construct()
	{
		parent::__construct();		
		$this->load->model('modelAuthentication');
		$this->load->helper('url');
	}	

	public function index()
	{	
		if($_POST)
		{
			$usr_email = $this->input->post("usr_email");
			$password = $this->input->post("password");

			$this->valid = $this->modelAuthentication->loginAuth($usr_email, $password);
			// var_dump($this->session->userdata()); exit();

			if($this->valid)
			{
				// var_dump($this->session->userdata("")); exit();
				$this->session->set_flashdata(array("type" => "success", "msg" => "Login successfully!"));
				redirect("/main/index");


			}
			else{
				$this->session->set_flashdata(array("type" => "warning", "msg" => "Login authentication failed!"));
				redirect("/main/login");
			}
		}

		$this->container["content"] = NULL;
		$this->twig->display("login3.html", $this->container);
	}
	
}
