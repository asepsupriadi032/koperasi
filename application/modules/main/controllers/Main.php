<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');		
		$this->load->helper('url');
		$this->load->model('modelAccess');
		$this->load->helper('token'); 

        $this->API = $this->config->item('base_url')."/main";	
		$this->tokenAPI = new TokenAPI();	
        
		LoggedSystem();		
	}	

	public function index()
	{	
		$this->container['hcmIdUser'] = $this->session->userdata("hcmIdUser");
   		$this->container['hcmUser'] = $this->session->userdata("hcmUser");
		$this->container["content"] = NULL;
		// var_dump( $this->session->userdata("hcmIdUser")); exit();
		$this->twig->display("home.html", $this->container);
	}

	public function logout(){
		$param['token'] = $this->tokenAPI->getToken();		
		$param['action'] = "logout";
		// $result =  json_decode($this->curl->simple_get($this->API.'/access', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		$obj = (object) $param;
		$result = $this->modelAccess->action($obj);
		// var_dump($result); exit();
		if($result['type'] == "success") {
		// if($result->type == "success") {
			$this->session->sess_destroy();
			$this->session->set_flashdata(array("type" => $result->type, "msg" => $result->msg));
		}
		
		redirect(base_url());	
	}
	
}
