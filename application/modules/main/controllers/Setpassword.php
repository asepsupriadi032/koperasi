<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setpassword extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;

	public function __construct(){
		parent::__construct();		
		$this->load->helper('accesscontrol');		
		$this->load->helper('url');
		$this->load->helper('token'); 
		$this->tokenAPI = new TokenAPI();
		$this->load->model('PasswordModel');	
        
		LoggedSystem();		
	}	

	public function index(){
		$usr_id =  $this->session->userdata("hcmIdUser");
		$param['token'] = $this->tokenAPI->getToken();
		$param['usr_id'] = $usr_id;

		$obj = (object) $param;
		$data = $this->PasswordModel->getData($obj);

		$this->container['usr_email'] = $data['row']->usr_email;
		$this->container['id_user'] = $usr_id;
		$this->twig->display('form/formSetPassword.html',$this->container);
		// echo "data";
	}

	public function editPassword(){

		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();			
		$param['id_user'] = $this->session->userdata("hcmIdUser"); //Disini lah user id yang akan di simpan di dalam log di inisialisasi
		$param['hcmUser'] = $this->session->userdata("hcmUser");
		$obj = (object) $param;
		if($param["usr_password"] == $param["usr_confirm"]){

			$data = $this->PasswordModel->cekData($obj);
			$rowdata = $data['row'];
			if($rowdata >0){
				$data2 = $this->PasswordModel->updatePass($obj);
			// var_dump($data2); exit();
				$this->session->set_flashdata(array("type" => $data2['type'], "msg" => $data2['msg']));
			}else{
				$this->session->set_flashdata(array("type" => "error", "msg" => "Password lama yang anda masukan salah"));
			}
		}else{
			$this->session->set_flashdata(array("type" => "error", "msg" => "gagal"));
		}
				
		redirect(base_url('main/Setpassword.html'));
	}
}
?>