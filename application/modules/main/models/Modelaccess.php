<?php
class modelAccess  extends CI_Model {

	var $output;
    var $menuLastVersion;

	public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->output = array();
        $this->load->helper('token');
        $this->tokenAPI = new TokenAPI();
	}

	public function action($obj){
        $output = array();



        //$this->response("sd");

        switch ($obj->action) {

            /* Handling Login */
           case 'login':
                $valid = false;
                $menuChecked = false;
                $password = md5($obj->password);
                $token = $obj->token;
                $query = $this->db->get_where("mnu_user", array("usr_email" => $obj->usr_email,"usr_password" => $password));
                if($query->num_rows() > 0)
                {
                   $data = $query->row();


                   // if($data->usr_username == (string) $obj->username and $data->usr_password == (string) $password)
                   // {
                   //      //$menuChecked = $this->checkLastMenu($obj->menu_ver);

                         $valid = $this->tokenAPI->checkToken($token);

                        $flag = 1;
                        $ustId = $data->ust_id;

                   //      $getVersion = $this->db->get_where("mnu_version", array("ver_name" => "hcms_menu", "ust_id" => $ustId))->row();

                   //      $menuVersion = $getVersion->ver_number;
                   // }


                }

                if($valid) {

                    $output = array("type" => "success", "msg" => "Login Success!", "data" => $data, "ust_id" => $ustId, "flag" => $flag);
                    // , "menu_version" => $menuVersion
                }
                else {
                    $output = array("type" => "error", "msg" => "Login Failed!");
                }

            break;
            /* End Handling Login */

            /* Handling Check Session */
            case 'check_session':

                $isLogin = $this->session->userdata('serviceHcmLogged');

                if(!$isLogin) {
                    $output = array("type" => "error", "msg" => "Session has expired!");
                }

            break;
            /* End Handling Check Session */

            /* Handling Logout */
            case 'logout':

               $this->session->sess_destroy();
               $output = array("type" => "success", "msg" => "Logout Success!");

            break;
            /* End Handling Logout */

            case 'request_menu':
                $ustId = $obj->ust_id;
                $menuList = $this->getLastMenu($ustId);
                $output = array("type" => "error", "msg" => "Getting Menu!", "menu_list" => $menuList);
            break;

        }

        return $output;
    }


    public function addLog($act, $table, $record, $id_user = NULL)
    {
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $record->id_user;
        }

        if ($act == "delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("id_user", $id_user);
        $this->db->set("action", $act);
        $this->db->set("action_date", date("Y-m-d H:i:s"));
        $this->db->set("record", $dataRecord);
        $this->db->set("table_transaction", $table);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }


    public function getLastMenu($ustId){
        $menuSql = "select a.*, b.nama_menu, b.parent_menu, b.url_menu, b.icon_menu, b.urutan from mnu_menu_role a join mnu_menu b on a.id_menu=b.id_menu where a.ust_id='".$ustId."'";
        //$menu = $this->db->get("mnu_menu")->result();
        $menu = $this->db->query($menuSql)->result();
        return $menu;
    }

    public function checkLastMenu($lastVersion){
        $upToDate = true;
        $last = $this->db->get_where("mnu_version", array("ver_name" => "hcms_menu"))->row();



        if($last->ver_number !== (int) $lastVersion) {
            $this->menuLastVersion = $last->ver_number;
            $upToDate = false;
        }

        return $upToDate;
    }

    public function getOrgAccess($ustId){
        $mnuorgaccess=false;
        // $this->db->select("ors_id");
        $mnuorgaccess = $this->db->get_where("mnu_ors_access", array("ust_id"=>$ustId))->result();

        $total_org_access = count($mnuorgaccess);
        $org_access = "";
        $i=1;
        $arrOrg = array();
        $x = 0;
        foreach($mnuorgaccess as $mnu_org){

            // $length = strlen("12");
            // $sql =" where substring(ors_structure, 1, ".$length.") = '".$mnu_org->ors_structure."'";
            // $this->db->where($sql);
            // $data = $this->db->get("mnu_ors_access")->result();
            // foreach($data as $row){

                $arrOrg[] = $mnu_org->ors_id;
            // }
        }

        // $this->db->select("ors_id");
        // $this->db->where("ust_id",$ustId);
        // $mnuorgaccess = $this->db->get("mnu_ors_access")->result();

        return $arrOrg;
    }


}
?>
