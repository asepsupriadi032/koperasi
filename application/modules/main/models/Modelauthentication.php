<?php
class modelAuthentication extends CI_Model {
	
    function __construct(){
        parent::__construct();        
        $this->load->helper('token'); 

        $this->API = $this->config->item('base_url')."main";	
        $this->load->model('modelAccess');
		$this->tokenAPI = new TokenAPI();	
    }
	
	public function loginAuth($usrEmail, $password)
	{
		$valid = false;		
		// $version = $this->db->get_where("mnu_version", array("ver_name" => "hcms_menu"))->row();

		$param['usr_email'] = $usrEmail;
		$param['password'] = $password;
		$param['token'] = $this->tokenAPI->getToken();		
		$param['action'] = "login";
		// $param['menu_ver'] = $version->ver_number;

		/* Check authentication dengan webservice, lalu set session jika valid */
		// $result =  json_decode($this->curl->simple_post($this->API.'/access', $param, array(CURLOPT_BUFFERSIZE => 10)));
		// var_dump($param);exit();
		$obj = (object) $param;
		$result = $this->modelAccess->action($obj);
		// var_dump($result); exit();

		/* Jika berhasil set session di webservice maka set session juga di client */
		if($result['type'] == "success") {
			// var_dump($result->data->usr_id); exit();
		// if($result->type == "success") {


			$session = array(
                'hcmLogged' => TRUE,
                'hcmUstId' => $result['data']->ust_id,
                'hcmIdUser' => $result['data']->usr_id,
                'hcmUser' => $result['data']->usr_name,
                'hcmPassword' => $result['data']->usr_password
                // 'hcmLevel' => $result->data->id_level
            );
            // $session = array(
            //     'hcmLogged' => TRUE,
            //     'hcmIdUser' => $result->data->usr_id,
            //     'hcmUser' => $result->data->usr_name,
            //     'hcmPassword' => $result->data->usr_password
            //     // 'hcmLevel' => $result->data->id_level
            // );

           
            
            $this->session->set_userdata($session); 

			// print_r($session); exit();
            $valid = true;	  
  
		}

		return $valid;		
	}	
	
	public function gantiPassword($password)
	{
		$log = $this->session->all_userdata();
		$userId = $this->session->userdata('userId');
		$valid = false; 
	
		$this->db->set("password", $password);
		$this->db->where("id_user", $userId);
		$valid = $this->db->update("mst_user");
			
		$session = array(
			  'userPassword' => $password,
		);
			
		$this->session->set_userdata($session);
		
		return $valid;
	}



	public function writeMenuLocally($updatedMenu, $lastVersion){
		$q = $this->db->query("delete from mnu_menu");
		
		foreach ($updatedMenu as $menu) {
			$this->db->set("id_menu", $menu->id_menu);
			$this->db->set("parent_menu", $menu->parent_menu);
			$this->db->set("nama_menu", trim($menu->nama_menu));
			$this->db->set("url_menu", trim($menu->url_menu));
			$this->db->set("icon_menu", trim($menu->icon_menu));
			$this->db->set("urutan", $menu->urutan);
			$this->db->set("status_aktif", $menu->status_aktif);
			$this->db->insert("mnu_menu");
		}
		
		$this->db->set("ver_number", $lastVersion);
		$this->db->where("ver_name", "hcms_menu");
		$this->db->update("mnu_version");
		
		return true;
	}
}
?>