<?php
class PasswordModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
        $this->output = array();	
		$this->tokenAPI = new TokenAPI();
    }

    public function getData($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
            $this->db->where('usr_id', $obj->usr_id);
            $data = $this->db->get("mnu_user")->row();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function cekData($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
            $row = 0;
            $pass = md5($obj->usr_pass);
            $usr_id = $obj->usr_id;
            $data = $this->db->query("SELECT * FROM mnu_user WHERE usr_id='".$usr_id."' and usr_password='".$pass."'")->row();
                   if($data){
                        $row=1;
                   }
            $output=array("type" => "success", "row" => $row);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function updatePass($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

            $usr_id = $obj->usr_id;
            $usr_email = $obj->usr_email;
            $pass = md5($obj->usr_password);

            $exec = $this->db->query("UPDATE mnu_user SET usr_email = '".$usr_email."', usr_password='".$pass."' , usr_chby='".$obj->hcmUser."', usr_chdt='". date('Y-m-d H:i:s')."' WHERE usr_id='".$usr_id."'");
            
            if($exec) {
                $output = array("type" => "success", "msg" => "Request success, data saved!");
            }
            else {
                $output = array("type" => "error", "msg" => "Request failed, please check the data!");
            }

        }
        else{
            $output = array("type" => "error", "msg" => "Invalid token!");
        }
        return $output;
    }

}