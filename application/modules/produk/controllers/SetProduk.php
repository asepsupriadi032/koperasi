<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SetProduk extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->load->helper('utility');	
		$this->container['data'] = null;
		
		// $this->API = $this->config->item('api_url')."/utility";
		$this->load->model('SetProdukModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridSetProduk.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->SetProdukModel->getKatProduk($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
					
					$bunga = "-";
					if ($row->kat_kategori == "1"){
						$bunga = $row->kat_bunga." %";
					}

					if($row->bun_kategori=="0"){
						$kategori = "Bulanan";
					}else{
						$kategori= "Harian";
					}
				$responce->data[] = array(
					$x, 
					"<a href='".base_url()."produk/setProduk/DetailListProduk/".$row->kat_id."'>".$row->kat_kode."</a>", 
					$row->sin_nama, 
					$row->kat_nama,
					katProduk($row->kat_kategori),
					$kategori,
					$bunga,
					$row->kat_desk,		 
					$row->kat_id//8
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addKategori($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			$obj = (object) $param;
			$result = $this->SetProdukModel->postKategori($obj);
			// var_dump($result); exit();
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('produk/SetProduk.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->SetProdukModel->getKatProduk($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}

		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->SetProdukModel->getKatInstansi($obj);
		$this->container['kat_instansi'] = $data['row'];
	
		$this->twig->display("form/formKategoriProduk.html", $this->container);
	}

	public function delKatProduk($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->SetProdukModel->delKatProduk($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('produk/SetProduk.html'));
	}

	public function getInstansi(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->SetProdukModel->getSubInstansi($obj);

		// $this->container['kat_instansi'] = $data['row'];
		echo json_encode(array("type" => $data['type'], "row"=>$data['row']));

	}

	public function getKategori(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->SetProdukModel->getKategori($obj);

		// $this->container['kat_instansi'] = $data['row'];
		echo json_encode(array("type" => $data['type'], "row"=>$data['row']));

	}

	public function DetailListProduk($kat_id=NULL){

		$this->container['kat_id'] = $kat_id;

		$this->twig->display("grid/gridDetailProduk.html", $this->container);
	}
}
