<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubProduk extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		
		// $this->API = $this->config->item('api_url')."/utility";
		$this->load->model('setProdukModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridSubProduk.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->setProdukModel->getKatProduk($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
					
				$responce->data[] = array(
					$x, 
					$row->kat_kode, 
					$row->kat_nama,
					$row->kat_desk,		 
					$row->kat_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addKategori($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			$obj = (object) $param;
			$result = $this->setProdukModel->postKategori($obj);
			// var_dump($result); exit();
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('produk/SetProduk.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->setProdukModel->getKatProduk($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}
	
		$this->twig->display("form/formKategoriProduk.html", $this->container);
	}

	public function delKatProduk($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->setProdukModel->delKatProduk($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('produk/SetProduk.html'));
	}
}
