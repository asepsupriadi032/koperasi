<?php
class SetProdukModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        $this->load->model('LogModel');
		$this->container['data'] = null;
        $this->output = array();	
		$this->tokenAPI = new TokenAPI();
    }

    public function getKatProduk($obj){
        $output=array();
        $tokenValid = true;

        if($tokenValid){
            $this->db->join('ins_sub_instansi','ins_sub_instansi.sin_id=kat_produk.sin_id');
            $data = $this->db->get('kat_produk')->result();

            if(!empty($obj->id)){
                $this->db->where('kat_produk.kat_id', $obj->id);
                $this->db->join('ins_sub_instansi','ins_sub_instansi.sin_id=kat_produk.sin_id');
                $data = $this->db->get('kat_produk')->row();
            }
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postKategori($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
                $this->db->set("kat_kode", $obj->kat_kode);
                $this->db->set("sin_id", $obj->sin_id);
                $this->db->set("kat_nama", $obj->kat_nama);
                $this->db->set("kat_kategori", $obj->kat_kategori);
                $this->db->set("bun_kategori", $obj->bun_kategori);
                $this->db->set("kat_bunga", $obj->kat_bunga);
                $this->db->set("kat_desk", $obj->kat_desk);
                if(!empty($obj->kat_id)) {
                    $action = "update";
     
                    $this->db->where('kat_id', $obj->kat_id);
                    $this->db->set("kat_chby", $this->session->userdata("hcmUser"));
                    $this->db->set("kat_chdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->update("kat_produk");
                }
                else {
                    $action = "insert";
                    $this->db->set("kat_crby", $this->session->userdata("hcmUser"));
                    $this->db->set("kat_crdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->insert('kat_produk');     
                }

                if($exec) {
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                $modul = "Produk";
                $usr_id = $this->session->userdata("hcmIdUser");
                $valid = $this->LogModel->addLog($action, $modul, "kat_produk", $obj, $usr_id);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function delKatProduk($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $action = "Delete";
            //save log
            $modul = "Produk";
            $usr_id = $this->session->userdata("hcmIdUser");
            $valid = $this->LogModel->addLog($action, $modul, "kat_produk", array("kat_id" => $obj->id), $usr_id);
            //and save log

            $this->db->where('kat_id', $obj->id);
            $exec = $this->db->delete('kat_produk');

            if($exec) {
                
                $output= array("type" => "success", "msg" => "Request success, data deleted!");
            }
            else {
                $output= array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }
        else{
            $output= array("type" => "error", "msg" => "Request failed, invalid token!");
        }

        return $output;
    }

    public function getKatInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $data = $this->db->get('ins_instansi')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function getSubInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $this->db->where("ins_id",$obj->ins_id);
        $data = $this->db->get('ins_sub_instansi')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function getKategori($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $this->db->where("sin_id",$obj->sin_id);
        $data = $this->db->get('kat_produk')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

}