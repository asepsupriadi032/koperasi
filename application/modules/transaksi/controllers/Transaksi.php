<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->load->helper('utility');	
		// $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->container['data'] = null;
		
		// $this->API = $this->config->item('api_url')."/utility";
		$this->load->model('TransaksiModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	

		$this->twig->display("grid/gridTransaksi.html", $this->container);
	}

	public function getAjaxData($kat_id = NULL){
		if(!empty($kat_id)){
			$param['kat_id'] = $kat_id;
		}
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->TransaksiModel->getTransaksi($obj);
		// var_dump($data); exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$kode = "";
				$nama = "";
				if(!empty($kat_id)){
					$kode = $row->kat_kode;
					$nama = $row->ang_nama;
				}else{
					$kode ="<a href='".base_url("transaksi/Transaksi/detailTransaksiProduk/".$row->kat_id)."'>".$row->kat_kode."</a>";
					$nama = "<a href='".base_url("transaksi/Transaksi/detailTransaksi/".$row->tran_id)."'>".$row->ang_nama."</a>";
				}	

				if($row->tran_status_operational !="3"){
					$status = "-";
				}else{
					$status = statusTransaksi($row->tran_status);
				}

				if($row->tran_status_operational=="2" || $row->tran_status_operational=="4"){
					$komen = ": <b>".$row->tran_komen."</b>";
				}else{
					$komen = "";
				}
				$responce->data[] = array(
					$x, 
					$row->tran_transaksi_id, 
					$kode,
					$nama,
					$row->tran_tgl_pengajuan,
					number_format($row->tran_nominal,2,',','.'),
					$row->tran_tgl_transfer,		 
					statusOperational($row->tran_status_operational).$komen,
					$status,
					$row->tran_id, //9
					$row->tran_status_operational,
					$row->tran_komen
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addTransaksi($id = NULL){

		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;	
		$data = $this->TransaksiModel->getIdTransaksi($obj);

		if($data['row']==""){
			$dateNow = date('dmy');
			$this->container['idTransaksi'] = $dateNow.'0001';
		}else{
			$digit = substr($data['row']->tran_transaksi_id, 6);

			$lastYear =  substr($data['row']->tran_transaksi_id, 4,2);
			$nowYear = date('y');
			$dateNow = date('dmy');

			if($nowYear>$lastYear){
				$this->container['idTransaksi'] = $dateNow.'0001';
			}else{
				$maxDigit=4;
				$lastId=$digit+1;
				$str = "";
		            for ($i=1; $i <= ($maxDigit - strlen($lastId)); $i++) { 
		                $str.="0";
		        }
		        $str.=$lastId;
		        $this->container['idTransaksi'] = $dateNow.$str;
			}	
		}

			
		$param['token'] = $this->tokenAPI->getToken();
		$data1 = $this->TransaksiModel->getLama($param);
		$this->container['bunga'] = $data1['row'];
		// var_dump($lastId); exit();

		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->menuModel->getMenu($obj);
			// $data = json_decode($this->curl->simple_get($this->API.'/menu', $param));
			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}
		// var_dump($result); exit();
	
		$this->twig->display("form/formTransaksi.html", $this->container);
	}


	public function saveByAjax(){

		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();; 
		$obj = (object) $param;
		// var_dump($obj->ang_id); exit();

		if($obj->ang_id==""){
			$this->session->set_flashdata(array("type" => "error", "msg" => "Request failed, please check the data!"));
	        echo json_encode(array("type" => "error", "msg" => "Request failed, please check the data!"));
		}else{
			$a=$obj->ang_limit;
			$b=$obj->tran_nominal;
			if( $b > $a ){
				// $this->session->set_flashdata(array("type" => "error", "msg" => "Request failed, please check the 'Nominal'!"));
	        	echo json_encode(array("type" => "error", "msg" => "Request failed, please check the 'Nominal'!"));
			}else{
				$result = $this->TransaksiModel->postTransaksi($obj);
				// var_dump($result);exit();
				$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
		        echo json_encode(array("type" => $result['type'], "msg" => $result['msg']));
			}
		}

	}

	public function deleteTransaksi($id){
		$param['tran_id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;	
		$data = $this->TransaksiModel->deleteTransaksi($obj); 

		$this->session->set_flashdata(array("type" => $data['type'], "msg" => $data['msg']));

        redirect(base_url('transaksi/Transaksi.html'));
	}

	public function editTransaksi(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		// var_dump($obj); exit();
		$data = $this->TransaksiModel->editTransaksi($obj);
		// var_dump($data); exit();

		echo json_encode(array("type" => $data['type'], "msg" => $data['msg']));
	}

	public function delTransaksi(){
		$param['tran_id'] = $this->input->post("tran_id");
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;	
		$data = $this->TransaksiModel->deleteTransaksi($obj); 
		
		echo json_encode(array("type" => $data['type'], "msg" => $data['msg']));
	}

	public function detailTransaksi($tran_id = NULL){

		$param['tran_id'] = $tran_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->TransaksiModel->getDetailTransaksi($obj);

		$this->container['edit'] = $data['row'];

		$param['token'] = $this->tokenAPI->getToken();
		$data1 = $this->TransaksiModel->getLama($param);
		$this->container['bunga'] = $data1['row'];

		// var_dump($data['row']); exit();
		$this->twig->display("form/formDetailTransaksi.html", $this->container);
	}

	public function detailTransaksiProduk($kat_id = NULL){
		$param['kat_id'] = $kat_id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->TransaksiModel->getDetailTransaksiProduk($obj);

		$this->container['row'] = $data['row'];
		$this->container['tgl'] = date_format(date_create($data['row']->kat_crdt), "m/d/Y");
		// var_dump($obj); exit();
		$this->twig->display("grid/gridDetailTransaksiProduk.html", $this->container);
	}

	public function showExportExcel($tag = NULL){
		$this->container['tag'] = $tag;
		$this->container['tran_tgl_pengajuan'] = $this->input->post('tran_tgl_pengajuan');
		$this->twig->display("grid/gridShowExportExcel.html", $this->container);
	}

	public function getAjaxData2($tran_tgl_pengajuan = NULL){
		// $param = $this->input->post();
		if(!empty($tran_tgl_pengajuan)){
			$param['tran_tgl_pengajuan'] = $tran_tgl_pengajuan;
		}
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->TransaksiModel->getTransaksi2($obj);
		// var_dump($obj); exit();

		$x = 0;
		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$kode = "";
				$nama = "";
				
				if($row->tran_status_operational !="3"){
					$status = "-";
				}else{
					$status = statusTransaksi($row->tran_status);
				}

				if($row->tran_status_operational=="2" || $row->tran_status_operational=="4"){
					$komen = ": <b>".$row->tran_komen."</b>";
				}else{
					$komen = "";
				}

				if($row->bun_kategori=="0"){
					$kategori = "Bulan";
				}else{
					$kategori= "Hari";
				}
				$responce->data[] = array(
					$x, 
					$row->tran_transaksi_id, 
					$row->kat_kode,
					ucwords($row->ang_nama),
					getTanggal($row->tran_tgl_pengajuan),
					$row->tran_kebutuhan,
					number_format($row->tran_nominal,0,',','.'),
					$row->tran_bulan." ".$kategori,
					$row->tran_bunga." %",		
					number_format($row->tran_perbulan,0,',','.'), 
					number_format($row->tran_total,0,',','.'),
					statusOperational($row->tran_status_operational).$komen,
					$status,
					$row->tran_id, //9
					$row->tran_status_operational,
					$row->tran_komen
				);
			}
		}		
		echo json_encode($responce);
	}

	public function exportExcel($tran_tgl_pengajuan = NULL){
		if(empty($tran_tgl_pengajuan)){
            $tran_tgl_pengajuan = date('Y-m-d');
        }
		// // Load plugin PHPExcel nya
	    // include APPPATH.'libraries/PHPExcel.php';
	    // $this->load->library('PHPExcel');
	    // Panggil class PHPExcel nya
		include APPPATH.'libraries/PHPExcel.php';
		include APPPATH.'libraries/PHPExcel/IOFactory.php';

	    $excel = new PHPExcel();
	    // var_dump("sad"); exit();
	    // Settingan awal fil excel
   		 $excel->getProperties()->setCreator('My Notes Code')
                ->setLastModifiedBy('My Notes Code')
                ->setTitle("Data Transaksi")
                ->setSubject("Transaksi")
                ->setDescription("Laporan Transaksi")
                ->setKeywords("Data Transaksi");
	    //Buat sebuah variabel untuk menampung pengaturan style dari header tabel
	    $style_col = array(
	      'font' => array('bold' => true), // Set font nya jadi bold
	      'alignment' => array(
	        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
	      )
	    );
	    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
	    $style_row = array(
	      'alignment' => array(
	        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
	      ),
	      'borders' => array(
	        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
	        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
	        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
	        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
	      )
	    );
	    $excel->setActiveSheetIndex(0)->setCellValue('A1', "Data Transaksi ".getTanggal($tran_tgl_pengajuan)); // Header
	    $excel->getActiveSheet()->mergeCells('A1:M1'); // Set Merge Cell pada kolom A1 sampai E1
	    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
	    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
	    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
	    // Buat header tabel nya pada baris ke 3
	    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('B3', "Id Transaksi"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('C3', "Produk");
	    $excel->setActiveSheetIndex(0)->setCellValue('D3', "Nama Anggota"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('E3', "Tanggal Pengajuan"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('F3', "Kebutuhan"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('G3', "Nominal"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('H3', "Lama Pinjaman"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('I3', "Bunga"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('J3', "Angsuran Perbulan"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('K3', "Total"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('L3', "Status Operational"); 
	    $excel->setActiveSheetIndex(0)->setCellValue('M3', "Status"); 
	    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
	    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
	    $excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);

	    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
	    // $param = $this->input->post();
	    $param['tran_tgl_pengajuan'] = $tran_tgl_pengajuan;
		$param['token'] = $this->tokenAPI->getToken();	
		$obj = (object) $param;
		$data = $this->TransaksiModel->getDataTransaksi($obj); 
		// var_dump($obj); exit();
	    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
	    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
	    // var_dump($data['row']); exit();
	    foreach($data['row'] as $row){ // Lakukan looping pada variabel siswa
	    	if($row->tran_status_operational !="3"){
				$status = "-";
			}else{
				$status = statusTransaksi($row->tran_status);
			}
			if($row->bun_kategori=="0"){
					$kategori = "Bulan";
				}else{
					$kategori= "Hari";
				}

	      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
	      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $row->tran_transaksi_id);
	      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $row->kat_kode);
	      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $row->ang_nama);
	      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, getTanggal($row->tran_tgl_pengajuan));
	      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $row->tran_kebutuhan);
	      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, number_format($row->tran_nominal,0,',','.'));
	      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $row->tran_bulan." ".$kategori);
	      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $row->tran_bunga." %");
	      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, number_format($row->tran_perbulan,0,',','.'));
	      $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, number_format($row->tran_total,0,',','.'));
	      $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, statusOperational($row->tran_status_operational));
	      $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $status);
	      
	      // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
	      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
	      $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row);
	      
	      $no++; // Tambah 1 setiap kali looping
	      $numrow++; // Tambah 1 setiap kali looping
	    }

	    // Set width kolom
	    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
	    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); 
	    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(15); 
	    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
	    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25); 
	    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
	    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
	    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
	    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
	    $excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
	    $excel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
    
	    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
	    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
	    // Set orientasi kertas jadi LANDSCAPE
	    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	    // Set judul file excel nya
	    // $excel->getActiveSheet(0)->setTitle("Laporan Data Transaksi".$tran_tgl_pengajuan);
	    $excel->setActiveSheetIndex(0);
	    // var_dump($data); exit();
	    // Proses file excel
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename="Data Transaksi PT. TASS INDONESIA NUSANTARA '.$tran_tgl_pengajuan.'.xlsx"'); // Set nama file excel nya
	    header('Cache-Control: max-age=0');
	    $write = IOFactory::createWriter($excel, 'Excel2007');
	    $write->save('php://output');
		// var_dump($data); exit();
	}
}
