<?php
class TransaksiModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        // $this->load->model('utility/LogModel');
		$this->container['data'] = null;
        $this->output = array();	
		$this->tokenAPI = new TokenAPI();
    }

    public function getTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

            $this->db->select("tran_transaksi.*, ang_anggota.ang_nama, kat_produk.kat_kode, kat_produk.kat_id");
            $this->db->order_by('tran_transaksi.tran_transaksi_id','DESC');
            $this->db->join("ang_anggota","ang_anggota.ang_id=tran_transaksi.ang_id ",'left');
            $this->db->join("ang_karyawan","ang_karyawan.ang_id=tran_transaksi.ang_id",'left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            if(!empty($obj->kat_id)){
                $this->db->where('kat_produk.kat_id',$obj->kat_id);
            }
            $data = $this->db->get("tran_transaksi")->result();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function getTransaksi2($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

            if(!empty($obj->tran_tgl_pengajuan)){
                $this->db->where('tran_transaksi.tran_tgl_pengajuan', $obj->tran_tgl_pengajuan);
            }
            $this->db->select("tran_transaksi.*, ang_anggota.ang_nama, kat_produk.kat_kode, kat_produk.kat_id, kat_produk.bun_kategori");
            $this->db->order_by('tran_transaksi.tran_transaksi_id','DESC');
            $this->db->join("ang_anggota","ang_anggota.ang_id=tran_transaksi.ang_id ",'left');
            $this->db->join("ang_karyawan","ang_karyawan.ang_id=tran_transaksi.ang_id",'left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $data = $this->db->get("tran_transaksi")->result();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function getLama($obj){
        $output=array();
        $token = $obj['token'];
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
            $this->db->order_by('bun_kategori','ASC');
            $this->db->order_by('bun_jumlah','ASC');
            $this->db->select("mst_bunga.bun_id, mst_bunga.bun_jumlah, mst_bunga.bun_persen, mst_bunga.bun_kategori");
            $data = $this->db->get("mst_bunga")->result();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

                $this->db->where('mst_bunga.bun_id',$obj->bun_id);
                $getBunga = $this->db->get('mst_bunga')->row();

                $getlama = $getBunga->bun_bulan;

                $bunga = $obj->tran_bunga;

                $tran_total = $obj->tran_nominal + ($obj->tran_nominal * $bunga /100);
                $tran_perbulan = ceil($tran_total / $getlama);

                $this->db->set("tran_transaksi_id", $obj->tran_transaksi_id);
                $this->db->set("ang_id", $obj->ang_id);
                $this->db->set("bun_id", $obj->bun_id);
                $this->db->set("tran_kebutuhan", $obj->tran_kebutuhan);
                $this->db->set("tran_nominal", $obj->tran_nominal);
                $this->db->set("tran_jatuh_tempo", $obj->tran_jatuh_tempo);
                $this->db->set("tran_bunga", $obj->tran_bunga);
                $this->db->set("tran_bulan", $getlama);
                $this->db->set("tran_sisa_bulan", $getlama);
                $this->db->set("tran_total", $tran_total);
                $this->db->set("tran_sisa", $tran_total);
                $this->db->set("tran_perbulan", $tran_perbulan);
                $this->db->set("tran_lock", "0");

                if(!empty($obj->tran_id)) {
                    $action = "Update";
                    
                    $this->db->where('tran_id', $obj->tran_id);
                    $this->db->set("tran_chdt", date('Y-m-d H:i:s'));
                    $this->db->set("tran_chby", $this->session->userdata("hcmUser"));
                    $exec = $this->db->update("tran_transaksi");
                    $tran_id = $obj->tran_id;
                }
                else {
                    $action = "Insert";
                    $this->db->set("tran_tgl_pengajuan", date('Y-m-d'));
                    $this->db->set("tran_crdt", date('Y-m-d H:i:s'));
                    $this->db->set("tran_crby", $this->session->userdata("hcmUser"));
                    $this->db->set("tran_status_operational", "1");
                    $this->db->set("tran_status", "2");
                    $exec = $this->db->insert('tran_transaksi'); 
                    // $tran_id = $this->db->insert_id();  

                    $updateLimit=$obj->ang_limit - $obj->tran_nominal;
                    $updateDana=$obj->ang_dana - $obj->tran_nominal;

                    $this->db->where('ang_anggota.ang_id',$obj->ang_id);
                    $this->db->set("ang_limit",$updateLimit);
                    $this->db->set("ang_dana",$updateDana);
                    $this->db->update("ang_anggota");  
                }

                if($exec) {
                    //Save Log Db
                    $modul = "Transaksi";
                    $usr_id = $this->session->userdata("hcmIdUser");
                    $valid = $this->addLog($action, $modul, "tran_transaksi", $obj, $usr_id);
                    // End Save Log
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function getIdTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
            $this->db->limit(1);
            $this->db->order_by('tran_transaksi_id','DESC');
            $this->db->select("tran_transaksi_id");
            $data = $this->db->get("tran_transaksi")->row();
            if(empty($data)){
                $output=array("type" => "success", "row" => ""); 
            }
            $output=array("type" => "success", "row" => $data);
        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function deleteTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){

            $this->db->where('tran_transaksi.tran_id', $obj->tran_id);
            $this->db->select('tran_transaksi.tran_nominal, tran_transaksi.ang_id, ang_anggota.ang_limit, ang_anggota.ang_dana');
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id');
            $getData = $this->db->get('tran_transaksi')->row();

            $ang_id = $getData->ang_id;
            $tran_nominal = $getData->tran_nominal;
            $ang_limit = $getData->ang_limit + $tran_nominal;
            $ang_dana = $getData->ang_dana + $tran_nominal;

            $this->db->where('ang_id',$ang_id);
            $this->db->set('ang_limit',$ang_limit);
            $this->db->set('ang_dana',$ang_dana);
            $this->db->update('ang_anggota');

            $action = "Delete";
            //save log
            $modul = "Transaksi";
            $usr_id = $this->session->userdata("hcmIdUser");
            $valid = $this->addLog($action, $modul, "tran_transaksi", array("tran_id" => $obj->tran_id), $usr_id);
            //and save log

            $this->db->where('tran_id',$obj->tran_id);
            $exec = $this->db->delete('tran_transaksi');
            if($exec) {
                
                $output= array("type" => "success", "msg" => "Request success, data deleted!");
            }
            else {
                $output= array("type" => "error", "msg" => "Request failed, please check the data!");
            }

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function editTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){ 
            $action = "Update"; 

            if($obj->tran_status_operational != "3"){
                $tran_status = 2;
            }else{
                
                $tran_status = 0;
                $this->db->where('angs_angsuran.tran_id',$obj->tran_id);
                $this->db->delete('angs_angsuran');

                $this->db->select('tran_transaksi.tran_bulan');
                $this->db->where('tran_transaksi.tran_id', $obj->tran_id);
                $getbulan = $this->db->get('tran_transaksi')->row();
                $bulan = $getbulan->tran_bulan;
                for($x=1; $x<=$bulan; $x++){
                    $this->db->set('tran_id',$obj->tran_id);
                    $this->db->insert('angs_angsuran');
                }
            }

            if(isset($obj->tran_tgl_transfer)){
                $this->db->set("tran_tgl_transfer", $obj->tran_tgl_transfer);
            }
            $this->db->set('tran_status',$tran_status);
            $this->db->set("tran_komen", $obj->tran_komen);
            $this->db->set("tran_status_operational", $obj->tran_status_operational);
            $this->db->set("tran_chby", $this->session->userdata("hcmUser"));
            $this->db->set("tran_chdt", date('Y-m-d H:i:s'));
            $this->db->where('tran_id', $obj->tran_id);
            $exec = $this->db->update("tran_transaksi");

            if($exec) {
                //Save Log Db
                $modul = "Transaksi";
                $usr_id = $this->session->userdata("hcmIdUser");
                $valid = $this->addLog($action, $modul, "tran_transaksi", $obj, $usr_id);
                // End Save Log
                $output=array("type" => "success", "msg" => "Request success, data saved!");
            }
            else {
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }

            //Save Log Db
            //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
            // End Save Log
        }else{
            $output=array("type" => "error", "msg" => "Request failed, invalid token!");
        }
        return $output;
    }

    public function getDetailTransaksi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){ 
            $this->db->select("tran_transaksi.*, ang_anggota.ang_nama, ang_anggota.ang_hp, ang_anggota.ang_limit, ang_anggota.ang_dana, ang_bank.bnk_nama, ang_bank.bnk_pemilik, ang_bank.bnk_norek, kat_produk.kat_kode, kat_produk.kat_nama");
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('ang_bank','ang_bank.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=tran_transaksi.ang_id','left');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id','left');
            $this->db->where('tran_transaksi.tran_id', $obj->tran_id);
            $exec = $this->db->get("tran_transaksi")->row();

            if($exec) {
                $output=array("type" => "success", "msg" => "Request success, data saved!" , "row" => $exec);
            }
            else {
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }

            //Save Log Db
            //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
            // End Save Log
        }else{
            $output=array("type" => "error", "msg" => "Request failed, invalid token!");
        }
        return $output;
    }

    public function getDetailTransaksiProduk($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){ 
            
            $this->db->where('kat_produk.kat_id', $obj->kat_id);
            $this->db->join('ins_sub_instansi','ins_sub_instansi.sin_id=kat_produk.sin_id','left');
            $exec = $this->db->get("kat_produk")->row();

            if($exec) {
                $output=array("type" => "success", "msg" => "Request success, data saved!" , "row" => $exec);
            }
            else {
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }

            //Save Log Db
            //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
            // End Save Log
        }else{
            $output=array("type" => "error", "msg" => "Request failed, invalid token!");
        }
        return $output;
    }

    public function addLog($act, $modul, $table, $record, $id_user = NULL){
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $this->session->userdata("hcmIdUser");
        }

        if ($act == "Delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("usr_id", $id_user);
        $this->db->set("log_action", $act);
        $this->db->set("log_date", date("Y-m-d H:i:s"));
        $this->db->set("log_record", $dataRecord);
        $this->db->set("log_table", $table);
        $this->db->set("log_modul", $modul);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }

    public function getDataTransaksi($obj){
        $output = array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $tran_tgl_pengajuan = $obj->tran_tgl_pengajuan;
        if(empty($obj->tran_tgl_pengajuan)){
            $tran_tgl_pengajuan = date('Y-m-d');
        }
        if ($tokenValid){

            $this->db->select('tran_transaksi.*, kat_produk.kat_kode, ang_anggota.ang_nama, kat_produk.bun_kategori');
            $this->db->where('tran_transaksi.tran_tgl_pengajuan', $tran_tgl_pengajuan);
            $this->db->join('ang_karyawan','ang_karyawan.ang_id=tran_transaksi.ang_id');
            $this->db->join('kat_produk','kat_produk.kat_id=ang_karyawan.kat_id');
            $this->db->join('ang_anggota','ang_anggota.ang_id=tran_transaksi.ang_id');
            $exec = $this->db->get('tran_transaksi')->result();

            if($exec){
                $output = array("type" => "success", "row" => $exec);
            }else{
                $output = array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }else{
            $output = array("type" => "error", "msg" => "Request failed, invalid token!");
        }

        return $output;
    }

}