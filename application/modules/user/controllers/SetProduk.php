<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SetProduk extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		
		// $this->API = $this->config->item('api_url')."/utility";
		$this->load->model('setProdukModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridSetProduk.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		// $data = json_decode($this->curl->simple_get($this->API.'/menu', $params));
		$data = $this->menuModel->getMenu($param['token']);
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$name="Root";
				if($row->mnu_parent !=='0'){
					$name = $row->parent_name;
				}
					
				$responce->data[] = array(
					$x, 
					$name, 
					$row->mnu_name,
					$row->mnu_url,
					$row->mnu_icon,
					$row->mnu_urut,
					$row->mnu_status,		 
					$row->mnu_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addMenu($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			// $data =  json_decode($this->curl->simple_post($this->API.'/menu', $param, array(CURLOPT_BUFFERSIZE => 10))); 
			$obj = (object) $param;
			$result = $this->menuModel->postMenu($obj);
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('utility/menu.html'));
		}

		$param['token'] = $this->tokenAPI->getToken();
		// $data1 = json_decode($this->curl->simple_get($this->API.'/menu', $param));
		$data1 = $this->menuModel->getMenu($param['token']);
		// var_dump($data1); exit();
		$this->container['parent_menu'] = $data1['row'];

		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->menuModel->getMenu($obj);
			// $data = json_decode($this->curl->simple_get($this->API.'/menu', $param));
			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}
		// var_dump($result); exit();
	
		$this->twig->display("form/formMenu.html", $this->container);
	}

	public function deleteMenu($id){
		// $param = array('token' => $this->tokenAPI->getToken(),'id_menu' => $id);

		// $data =  json_decode($this->curl->simple_delete($this->API.'/menu', $param, array(CURLOPT_BUFFERSIZE => 10)));
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		// $data =  json_decode($this->curl->simple_post($this->API.'/menu', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		$obj = (object) $param;
		$result = $this->menuModel->delMenu($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('utility/menu.html'));
	}
}
