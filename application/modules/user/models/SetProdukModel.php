<?php
class SetProdukModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
        $this->output = array();

		
		// $this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getMenu($obj){
        $output=array();
        // $token = $this->input->get($obj);
        // $tokenValid = $this->tokenAPI->checkToken($token);
        $tokenValid = true;

        if($tokenValid){

            // $id = $this->get('id');
            $this->db->select("mnu_menu.*, parent.mnu_name as parent_name");
            $this->db->order_by('mnu_menu.mnu_parent','ASC');
            $this->db->join("mnu_menu as parent","mnu_menu.mnu_parent=parent.mnu_id","left");
            $data = $this->db->get('mnu_menu')->result();

        // var_dump($data); exit();

            if(!empty($obj->id)){
                $this->db->where('mnu_menu.mnu_id', $obj->id);
                $this->db->select("mnu_menu.*, parent.mnu_name as parent_name");
                $this->db->order_by('mnu_menu.mnu_parent','ASC');
                $this->db->join("mnu_menu as parent","mnu_menu.mnu_parent=parent.mnu_id","left");
                $data = $this->db->get('mnu_menu')->row();
            }
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postMenu($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
                $this->db->set("mnu_parent", $obj->parent_menu);
                $this->db->set("mnu_name", $obj->nama_menu);
                $this->db->set("mnu_url", $obj->url_menu);
                $this->db->set("mnu_icon", $obj->icon_menu);
                $this->db->set("mnu_urut", $obj->urutan);              
                $this->db->set("mnu_status", $obj->status_aktif);

                if(!empty($obj->id_menu)) {
                    $action = "update";
     
                    $this->db->where('mnu_id', $obj->id_menu);
                    $exec = $this->db->update("mnu_menu");
                }
                else {
     
                    $exec = $this->db->insert('mnu_menu');     
                }

                if($exec) {
                    $update = $this->db->query("update mnu_version set ver_number=ver_number+1 where ver_name='hcms_menu'");
                    // $update = "update mnu_version set [ver_number]=[ver_number] + 1 where [ver_name]='hcms_menu'";
                    // $execStmt = $this->db->query($update);

                    $output=array("type" => "success", "msg" => "Request success, data saved!");
                    //$this->response(array('status' => 'success'), 201);
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function delMenu($obj){
        // $obj = (object) $this->delete();
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        // var_dump($obj); exit();

        if($tokenValid){

            $this->db->where('mnu_id', $obj->id);
            $exec = $this->db->delete('mnu_menu');

            if($exec) {
                // $update = "update mnu_version set [ver_number]=[ver_number] + 1 where [ver_name]='hcms_menu'";
                // $execStmt = $this->db->query($update);
                
                $output= array("type" => "success", "msg" => "Request success, data deleted!");
            }
            else {
                $output= array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }
        else{
            $output= array("type" => "error", "msg" => "Request failed, invalid token!");
        }

        return $output;
    }

}