<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		$this->load->model('BankModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index(){	
		$this->twig->display("grid/gridBank.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->BankModel->getBank($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
					
				$responce->data[] = array(
					$x,  
					$row->bank_nama,
					$row->bank_desk,		 
					$row->bank_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addBank($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			// var_dump($param); exit();
			$obj = (object) $param;
			$result = $this->BankModel->postBank($obj);
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('utility/Bank.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->BankModel->getBank($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}

	
		$this->twig->display("form/formAddBank.html", $this->container);
	}

	public function delBank($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->BankModel->delBank($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('utility/Bank.html'));
	}
}
