<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bunga extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		$this->load->model('BungaModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index(){	
		$this->twig->display("grid/gridBunga.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->BungaModel->getBunga($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				
				if($row->bun_kategori=="0"){
					$kategori = "Bulan";
				}else{
					$kategori= "Hari";
				}
				$responce->data[] = array(
					$x,  
					$kategori,
					$row->bun_jumlah." ".$kategori,
					// $row->bun_persen ."(%)",		 
					$row->bun_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addBunga($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			// var_dump($param); exit();
			$obj = (object) $param;
			$result = $this->BungaModel->postBunga($obj);
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('utility/Bunga.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->BungaModel->getBunga($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}

	
		$this->twig->display("form/formAddBunga.html", $this->container);
	}

	public function delBunga($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->BungaModel->delBunga($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('utility/Bunga.html'));
	}
}
