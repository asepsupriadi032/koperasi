	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorit extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null; 
		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function saveByAjax(){
		// $url=strlen(base_url());
		// $fav_url= 'http://localhost:8080/hcms/employee/Contract';
		// // $fav_url=$this->input->post('fav_url');
		// $hasil=substr($fav_url, $url);
		// echo $hasil;

		// $param=$this->input->post();
		// $param['chartotal']=strlen(base_url());
		// $param['action']="addfavorituser";
  //       $param['id_user'] = $this->session->userdata("hcmIdUser"); 
  //       $param['hcmUser'] = $this->session->userdata("hcmUser");
		// $param['token'] = $this->tokenAPI->getToken();
		// $data = json_decode($this->curl->simple_post($this->API.'/Favorit', $param));
		$chartotal=strlen(base_url());
		$fav_url=substr($this->input->post('fav_url'),$chartotal);
        $this->db->set('fav_link',$fav_url);
        $this->db->set('usr_id',$this->session->userdata("hcmIdUser"));
        $this->db->set('fav_name',$this->input->post('fav_name'));
        $exec=$this->db->insert('fav_user');

        $usr_id=$this->session->userdata('hcmIdUser');
	    $this->db->where('fav_user.usr_id',$usr_id);
	    $row=$this->db->get('fav_user')->result();
		// var_dump($exec);
		// exit;
		if($exec) {
        	echo json_encode(array("type" => "success", "msg" => "Request success, Favorite saved!", "row" => $row));
        //$this->response(array('status' => 'success'), 201);
        }else {
            echo json_encode(array("type" => "error", "msg" => "Request failed, please check the data!"));
        }
		// echo json_encode(array("type" =>"success", "msg"=>$data->msg));
	}

	public function geturlactive(){

		// print_r($_POST);
		// var_dump($data);
		// exit;
		$chartotal=strlen(base_url());
		$fav_link=substr($this->input->post('url_active'),$chartotal);

		$this->db->where('fav_user.usr_id',$this->input->post('usr_id'));
		$this->db->where('fav_user.fav_link',$fav_link);
		$data=$this->db->get('fav_user')->row();
		if(!empty($data)){
			$row=array("active"=>"1", "fav_id"=>$data->fav_id);
		}else{
			$row=array("active"=>"0");
		}
		// print_r($row);
		// exit();
		echo json_encode(array("type" => "success", "row"=>$row));
	}


	public function deleteurlfav(){

		$this->db->where('fav_user.fav_id',$this->input->post('fav_id'));
		$exec = $this->db->delete('fav_user');

		$usr_id=$this->session->userdata('hcmIdUser');
	    $this->db->where('fav_user.usr_id',$usr_id);
	    $row=$this->db->get('fav_user')->result();

		if($exec){
			echo json_encode(array("type"=>"success", "msg"=>"Request success, data deleted!", "row"=>$row));
		}else{
			echo json_encode(array("type" => "error", "msg" => "Request failed, please check the data!"));
		}
	}
}
