<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instansi extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		$this->load->model('instansiModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index(){	
		$this->twig->display("grid/gridinstansi.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->instansiModel->getInstansi($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
					
				$responce->data[] = array(
					$x,  
					$row->ins_nama,
					$row->ins_desk,		 
					$row->ins_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addInstansi($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			$obj = (object) $param;
			$result = $this->instansiModel->postInstansi($obj);
			// var_dump($result); exit();
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('utility/Instansi.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->instansiModel->getInstansi($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}
	
		$this->twig->display("form/formInstansi.html", $this->container);
	}

	public function delInstansi($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->instansiModel->delInstansi($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('utility/Instansi.html'));
	}
}
