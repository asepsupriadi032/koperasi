<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridNotification.html", $this->container);
	}


	public function getAjaxData(){
		$params = array('token' => $this->tokenAPI->getToken());
		$data = json_decode($this->curl->simple_get($this->API.'/Notification', $params));
		
		$x = 0;

		if(empty($data->row)){
			$responce = NULL;
		}
		
		foreach($data->row as $row) { 
			$x++;
			$responce->data[] = array(
				$x, 
				$row->nama_module,
				$row->nama_submodule,
				$row->not_startingtime,
				$row->not_iterance,
				$row->not_id
			);
		}
		
		echo json_encode($responce);
	}	

	


	public function addNotification($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();			
			$param['id_user'] = $this->session->userdata("hcmIdUser"); //Disini lah user id yang akan di simpan di dalam log di inisialisasi
			

			$data =  json_decode($this->curl->simple_post($this->API.'/Notification', $param, array(CURLOPT_BUFFERSIZE => 10))); 
			$this->session->set_flashdata(array("type" => $data->type, "msg" => $data->msg));
			
			redirect(base_url('utility/Notification.html'));
		}

		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$data = json_decode($this->curl->simple_get($this->API.'/Notification', $param));
			
			$this->container['edit'] = $data->row;
			$this->container['id']	= $id;
		}
	
		$this->twig->display("form/formNotification.html", $this->container);
	}

	public function deleteNotification($id){
		$param = array('token' => $this->tokenAPI->getToken(),'sco_id' => $id);
		$param['id_user'] = $this->session->userdata("hcmIdUser");

		$data =  json_decode($this->curl->simple_delete($this->API.'/Notification', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		$this->session->set_flashdata(array("type" => $data->type, "msg" => $data->msg));

        redirect(base_url('utility/Notification.html'));
	}
}
