<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roleaccess extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	 
		$this->load->model('MenuRoleModel');
		$this->container['data'] = null;
		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
    }
    
    public function index(){	
		$this->twig->display("grid/RoleAccess.html", $this->container);
	}

	public function addRoleAccess($usrId = NULL){
		
        $token = $this->tokenAPI->getToken();
		$params = array('token' => $token, 'action'=>'getuser', 'usr_id' => $usrId);	
		$data = $this->MenuRoleModel->getUser($params);

        $par = array('token' => $token, 'action'=>'all_menu', 'usr_id' => $usrId);	
		$menu = $this->MenuRoleModel->getAllmenu($par);

        $par2 = array('token' => $token,  'usr_id' => $usrId);  
        $produk = $this->MenuRoleModel->getAllProduk($par);
        // var_dump($produk); exit();

        $this->container['data_menu']=$menu['row'];
        $this->container['data_produk']=$produk['row'];
        $this->container['usr_id']=$usrId;
        $this->container['user_type_name']=$data['row']->usr_name;
        
		$this->twig->display("grid/gridRoleAccess.html", $this->container);
	}

	public function getAjaxData(){
		$params = array('token' => $this->tokenAPI->getToken());
		$data = $this->MenuRoleModel->getUser($params);

		$x = 0;

		if(empty($data{'row'})){
			$responce = 0;
		}
		
		foreach($data['row'] as $row) { 
			$x++;
			$status = "";

			if($row->usr_status=='1'){
				$status = "Aktif";
			}else{
				$status= "Tidak Aktif";
			}
			$responce->data[] = array(
				$x, 
				$row->usr_name,
				$row->usr_email,
				$row->usr_hp,
				$status,
				$row->usr_id
			);
		}
		
		echo json_encode($responce);
	}	

	
   

    public function getMenuRole($ustId = NULL){
		$str ="";
		$str .="<ul id='tree'>";
        $parent0 = $this->Menu_role->getMenu(0, $ustId);
        
        
	    foreach ($parent0->data as $parent_0) {
            $id_parent0 = $parent_0->id_menu;
            

            $parent1 = $this->Menu_role->getMenu($id_parent0, $ustId);
            //var_dump($parent1->jumlah);
            //exit();

	    	if($parent1->jumlah <= 0){
	    		//level 1
                $str .= "<li><input type='checkbox' class='checkRole' name='id_menu[]' value='".$id_parent0."'"; if($id_parent0 == $parent_0->menu_id) { $str.="checked"; } $str.= ">".$parent_0->nama_menu."</li>";
                $str .="<input type='hidden' name='mrl_create".$id_parent0."' id='mrl_create".$id_parent0."' value='Y'>
                        <input type='hidden' name='mrl_update".$id_parent0."' id='mrl_update".$id_parent0."' value='Y'>
                        <input type='hidden' name='mrl_delete".$id_parent0."' id='mrl_delete".$id_parent0."' value='Y'>
                        <input type='hidden' name='mrl_read".$id_parent0."' id='mrl_read".$id_parent0."' value='Y'>";
            }
            else{
                //level 1
                $str .= "<li><input type='checkbox' class='checkRole' name='id_menu[]' value='".$id_parent0."'"; if($id_parent0 == $parent_0->menu_id) { $str.="checked"; } $str.= ">".$parent_0->nama_menu;
                $str .="<input type='hidden' name='mrl_create".$id_parent0."' id='mrl_create".$id_parent0."' value='Y'>
                        <input type='hidden' name='mrl_update".$id_parent0."' id='mrl_update".$id_parent0."' value='Y'>
                        <input type='hidden' name='mrl_delete".$id_parent0."' id='mrl_delete".$id_parent0."' value='Y'>
                        <input type='hidden' name='mrl_read".$id_parent0."' id='mrl_read".$id_parent0."' value='Y'>";
                $str .="<ul>";
                    foreach ($parent1->data as $parent_1) {
                        $id_parent1=$parent_1->id_menu;

                        $parent2=$this->Menu_role->getMenu($id_parent1, $ustId);
                        if($parent2->jumlah <= 0){
                            $str .= "<li>
                                            <table>
                                                <tr>
                                                    <td style='width:295px;' cellpadding='4'>
                                                        <input type='checkbox' class='checkRole' name='id_menu[]' value='".$id_parent1."'"; if($id_parent1 == $parent_1->menu_id) { $str.="checked"; } $str.= ">".$parent_1->nama_menu."
                                                    </td>
                                                    <td>
                                                        <select id='mrl_create".$id_parent1."' name='mrl_create".$id_parent1."'>
                                                            <option value='Y'"; if($parent_1->mrl_create == 'Y') { $str.="selected"; } $str.=">Allow to Create</option>
                                                            <option value='N'"; if($parent_1->mrl_create == 'N') { $str.="selected"; } $str.=">No</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id='mrl_update".$id_parent1."' name='mrl_update".$id_parent1."'>
                                                            <option value='Y'"; if($parent_1->mrl_update == 'Y') { $str.="selected"; } $str.=">Allow to Edit</option>
                                                            <option value='N'"; if($parent_1->mrl_update == 'N') { $str.="selected"; } $str.=">No</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id='mrl_delete".$id_parent1."' name='mrl_delete".$id_parent1."'>
                                                            <option value='Y'"; if($parent_1->mrl_delete == 'Y') { $str.="selected"; } $str.=">Allow to Delete</option>
                                                            <option value='N'"; if($parent_1->mrl_delete == 'N') { $str.="selected"; } $str.=">No</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id='mrl_read".$id_parent1."' name='mrl_read".$id_parent1."'>
                                                            <option value='Y'"; if($parent_1->mrl_read == 'Y') { $str.="selected"; } $str.=">Allow to View</option>
                                                            <option value='N'"; if($parent_1->mrl_read == 'N') { $str.="selected"; } $str.=">No</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>


                                </li>";
                        }
                        else
                        {
                            $str .= "<li><input type='checkbox' class='checkRole' name='id_menu[]' value='".$id_parent1."'"; if($id_parent1 == $parent_1->menu_id) { $str.="checked"; } $str.= ">".$parent_1->nama_menu;
                            $str .="<input type='hidden' name='mrl_create".$id_parent1."' id='mrl_create".$id_parent1."' value='Y'>
                                    <input type='hidden' name='mrl_update".$id_parent1."' id='mrl_update".$id_parent1."' value='Y'>
                                    <input type='hidden' name='mrl_delete".$id_parent1."' id='mrl_delete".$id_parent1."' value='Y'>
                                    <input type='hidden' name='mrl_read".$id_parent1."' id='mrl_read".$id_parent1."' value='Y'>";
                            $str .="<ul>";
                                        foreach ($parent2->data as $parent_2) {
                                            $id_parent2 = $parent_2->id_menu;
                                            $parent3=$this->Menu_role->getMenu($id_parent2, $ustId);

                                            if($parent3->jumlah <=0 )
                                            {
                                                $str .= "<li>
                                                        <table>
                                                            <tr>
                                                                <td style='width:250px;' cellpadding='4'>
                                                                    <input type='checkbox' class='checkRole' name='id_menu[]' value='".$id_parent2."'"; if($id_parent2 == $parent_2->menu_id) { $str.="checked"; } $str.= ">".$parent_2->nama_menu."
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_create".$id_parent2."' name='mrl_create".$id_parent2."'>
                                                                        <option value='Y'"; if($parent_2->mrl_create == 'Y') { $str.="selected"; } $str.=">Allow to Create</option>
                                                                        <option value='N'"; if($parent_2->mrl_read == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_update".$id_parent2."' name='mrl_update".$id_parent2."'>
                                                                        <option value='Y'"; if($parent_2->mrl_update == 'Y') { $str.="selected"; } $str.=">Allow to Edit</option>
                                                                        <option value='N'"; if($parent_2->mrl_update == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_delete".$id_parent2."' name='mrl_delete".$id_parent2."'>
                                                                        <option value='Y'"; if($parent_2->mrl_delete == 'Y') { $str.="selected"; } $str.=">Allow to Delete</option>
                                                                        <option value='N'"; if($parent_2->mrl_delete == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_read".$id_parent2."' name='mrl_read".$id_parent2."'>
                                                                        <option value='Y'"; if($parent_2->mrl_read == 'Y') { $str.="selected"; } $str.=">Allow to View</option>
                                                                        <option value='N'"; if($parent_2->mrl_read == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>";
                                            }
                                            else
                                            {
                                                foreach ($parent3->data as $parent_3) { 
                                                    $id_parent3 = $parent_3->id_menu;
                                                    $str .= "<li>
                                                        <table>
                                                            <tr>
                                                                <td style='width:250px;' cellpadding='4'>
                                                                    <input type='checkbox' class='checkRole' name='id_menu[]' value='".$id_parent3."'"; if($id_parent3 == $parent_3->menu_id) { $str.="checked"; } $str.= ">".$parent_3->nama_menu."
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_create".$id_parent3."' name='mrl_create".$id_parent3."'>
                                                                        <option value='Y'"; if($parent_3->mrl_create == 'Y') { $str.="selected"; } $str.=">Allow to Create</option>
                                                                        <option value='N'"; if($parent_3->mrl_read == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_update".$id_parent3."' name='mrl_update".$id_parent3."'>
                                                                        <option value='Y'"; if($parent_3->mrl_update == 'Y') { $str.="selected"; } $str.=">Allow to Edit</option>
                                                                        <option value='N'"; if($parent_3->mrl_update == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_delete".$id_parent3."' name='mrl_delete".$id_parent3."'>
                                                                        <option value='Y'"; if($parent_3->mrl_delete == 'Y') { $str.="selected"; } $str.=">Allow to Delete</option>
                                                                        <option value='N'"; if($parent_3->mrl_delete == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select id='mrl_read".$id_parent3."' name='mrl_read".$id_parent3."'>
                                                                        <option value='Y'"; if($parent_3->mrl_read == 'Y') { $str.="selected"; } $str.=">Allow to View</option>
                                                                        <option value='N'"; if($parent_3->mrl_read == 'N') { $str.="selected"; } $str.=">No</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </li>";
                                                }
                                            }
                                            
                                        }	
                                $str .="</ul>";	
                            $str .="</li>";	
                        }

                    }
                $str .="</ul></li>";
	    			
	    	}
	    }
		     
		$str ."</ul>";
        
	    //var_dump($str);
	    //exit();

	    return $str;
	}

	public function inputAccessMenu(){
		$param = $this->input->post();
		$param['token'] = $this->tokenAPI->getToken();	
		$param['action']="saveRoleMenu";		
		$param['id_user'] = $this->session->userdata("hcmIdUser"); //Disini lah user id yang akan di simpan 
		$data= $this->MenuRoleModel->postMenuRole($param); 
        // var_dump($data); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
		redirect(base_url('utility/Roleaccess/'));
	}


}