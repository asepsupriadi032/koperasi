<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setuser extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->load->helper('utility');	
		$this->container['data'] = null; 
		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridsetuser.html", $this->container);
	}

	public function getAjaxData(){
		$params = array('token' => $this->tokenAPI->getToken(), 'action'=>'getlist');
		$data = json_decode($this->curl->simple_get($this->API.'/Setuser', $params));
		
		// var_dump($data);
		// exit();
		$x = 0;

		if(empty($data->row)){
			$responce = 0;
		}
		
		foreach($data->row as $row) { 
			$x++;
			$responce->data[] = array(
				$x, 
				$row->ust_user_type, 
				$row->name, 
				usertype_status($row->status),
				getashcd($row->as_hcd),
				$row->usr_id
			);
		}
		
		echo json_encode($responce);
	}


	public function addUser($usr_id=NULL){

        if($_POST){
            $param = $this->input->post();
            $param['token'] = $this->tokenAPI->getToken();          
            $param['id_user'] = $this->session->userdata("hcmIdUser"); //Disini lah user id yang akan di simpan di dalam log di inisialisasi

        	$param['hcmUser'] = $this->session->userdata("hcmUser"); 
            // print_r($param);
            // exit();
            $data =  json_decode($this->curl->simple_post($this->API.'/Setuser', $param, array(CURLOPT_BUFFERSIZE => 10))); 
            
            $this->session->set_flashdata(array("type" => $data->type, "msg" => $data->msg));
            
            redirect(base_url('utility/Setuser.html'));
        }

        if(!empty($usr_id)) {
            $param["usr_id"] = $usr_id;
            $param['token'] = $this->tokenAPI->getToken();
            $param['action']="getUser";
            $data = json_decode($this->curl->simple_get($this->API.'/Setuser', $param));
            $this->container['edit'] = $data->row;
            $this->container['usr_id2']  = $usr_id;
        }

        $params = array('token' => $this->tokenAPI->getToken(), 'action'=>'getUserType');
		$data = json_decode($this->curl->simple_get($this->API.'/Setuser', $params));

		$this->container['Usertype']=$data->row;
        $this->twig->display("form/formsetuser.html", $this->container);
        
    }

    public function deleteUser($usr_id){
		$param = array('token' => $this->tokenAPI->getToken(),'usr_id' => $usr_id);
		$param['id_user'] = $this->session->userdata("hcmIdUser");

		$data =  json_decode($this->curl->simple_delete($this->API.'/Setuser', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		$this->session->set_flashdata(array("type" => $data->type, "msg" => $data->msg));

        redirect(base_url('utility/Setuser.html'));
	}

}