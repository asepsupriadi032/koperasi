<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShowMenu extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridShowMenu.html", $this->container);
	}


	public function getAjaxData(){
		$params = array('token' => $this->tokenAPI->getToken());
		$data = json_decode($this->curl->simple_get($this->API.'/ShowMenu', $params));
		
		$x = 0;

		if(empty($data->row)){
			$responce = NULL;
		}
		
		foreach($data->row as $row) { 
			$x++;
			$responce->data[] = array(
				$x,
				$row->nama_menu,
				$row->url_menu,
				$row->id_menu
			);
		}
		
		echo json_encode($responce);
	}	
}
