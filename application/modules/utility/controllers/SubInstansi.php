<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubInstansi extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		$this->load->model('SubInstansiModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index(){	
		$this->twig->display("grid/gridSubInstansi.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->SubInstansiModel->getSubInstansi($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
					
				$responce->data[] = array(
					$x,  
					$row->ins_nama,
					$row->sin_kode,		 
					$row->sin_nama,		 
					$row->sin_alamat,		 
					$row->sin_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addSubInstansi($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			// var_dump($param); exit();
			$obj = (object) $param;
			$result = $this->SubInstansiModel->postSubInstansi($obj);
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('utility/SubInstansi.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->SubInstansiModel->getSubInstansi($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}

		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->SubInstansiModel->getKatInstansi($obj);
		$this->container['kat_instansi'] = $data['row'];
		// var_dump($data);exit();
	
		$this->twig->display("form/formSubInstansi.html", $this->container);
	}

	public function delSubInstansi($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->SubInstansiModel->delSubInstansi($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('utility/SubInstansi.html'));
	}
}
