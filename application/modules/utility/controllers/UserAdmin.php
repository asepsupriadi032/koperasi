<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserAdmin extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		$this->load->model('UserAdminModel');	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index(){	
		$this->twig->display("grid/gridUserAdmin.html", $this->container);
	}

	public function getAjaxData(){
		$param['token'] = $this->tokenAPI->getToken();	
		$data = $this->UserAdminModel->getUserAdmin($param['token']);
		// var_dump($data);exit();
		$x = 0;

		if(empty($data['row'])){
			$responce->data[] = 'error';
			echo json_encode($responce);

			return;
		}else{

			foreach($data['row'] as $row) { 
				$x++;
				$userType="";
				if($row->ust_id=='1'){
					$userType="Super Admin";
				}else{
					$userType="User Admin";
				}
				$responce->data[] = array(
					$x,  
					$userType,
					$row->usr_name,		 
					$row->usr_email,		 
					$row->usr_hp,		 
					$row->usr_id
				);
			}
		}		
		echo json_encode($responce);
	}	

	
	public function addUserAdmin($id = NULL){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();
			$obj = (object) $param;
			$result = $this->UserAdminModel->postUserAdmin($obj);
			$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));
			
			redirect(base_url('utility/UserAdmin.html'));
		}


		if(!empty($id)) {
			$param["id"] = $id;
			$param['token'] = $this->tokenAPI->getToken();

			$obj = (object) $param;
			$result = $this->UserAdminModel->getUserAdmin($obj);			
			$this->container['edit'] = $result['row'];
			$this->container['id']	= $id;
		}

		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$data = $this->UserAdminModel->getUserType($obj);
		$this->container['userType'] = $data['row'];
		// var_dump($data);exit();
	
		$this->twig->display("form/formUserAdmin.html", $this->container);
	}

	public function delSubInstansi($id){
		$param['id'] = $id;
		$param['token'] = $this->tokenAPI->getToken();
		$obj = (object) $param;
		$result = $this->UserAdminModel->delSubInstansi($obj); 
		// var_dump($result); exit();
		$this->session->set_flashdata(array("type" => $result['type'], "msg" => $result['msg']));

        redirect(base_url('utility/SubInstansi.html'));
	}
}
