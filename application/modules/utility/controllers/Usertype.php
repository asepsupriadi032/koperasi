<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype extends MX_Controller {

	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;

	public function __construct()
	{
		parent::__construct();		
		$this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();	

		LoggedSystem();		
	}

	public function index()
	{	
		$this->twig->display("grid/gridUserType.html", $this->container);
	}

	public function addUserType($ust_id =null){

		if($_POST){
			$param = $this->input->post();
			$param['token'] = $this->tokenAPI->getToken();			
			$param['id_user'] = $this->session->userdata("hcmIdUser"); //Disini lah user id yang akan di simpan di dalam log di inisialisasi
			// print_r($param);
			// exit;

			$data =  json_decode($this->curl->simple_post($this->API.'/Usertype', $param, array(CURLOPT_BUFFERSIZE => 10))); 
			$this->session->set_flashdata(array("type" => $data->type, "msg" => $data->msg));
			// var_dump($param); exit();
			
			redirect(base_url('utility/Usertype.html'));
		}

		if(!empty($ust_id)) {
			$param["ust_id"] = $ust_id;
			$param['token'] = $this->tokenAPI->getToken();

			$data = json_decode($this->curl->simple_get($this->API.'/Usertype', $param));
			$this->container['edit'] = $data->row;
			$this->container['ust_id']	= $ust_id;
		}

		$this->twig->display("form/formUserType.html", $this->container);
	}

	public function getAjaxData(){
		$params = array('token' => $this->tokenAPI->getToken());
		$data = json_decode($this->curl->simple_get($this->API.'/Usertype', $params));
		
		$x = 0;

		if(empty($data->row)){
			$responce = 0;
		}
		
		foreach($data->row as $row) { 
			$x++;
			$responce->data[] = array(
				$x, 
				$row->ust_user_type, 
				$row->ust_desc,
				$row->ust_id
			);
		}
		
		echo json_encode($responce);
	}	

	public function deleteUserType($ust_id){
		$param = array('token' => $this->tokenAPI->getToken(),'ust_id' => $ust_id);
		$param['id_user'] = $this->session->userdata("hcmIdUser");

		$data =  json_decode($this->curl->simple_delete($this->API.'/Usertype', $param, array(CURLOPT_BUFFERSIZE => 10))); 
		$this->session->set_flashdata(array("type" => $data->type, "msg" => $data->msg));

        redirect(base_url('utility/Usertype.html'));
	}

}