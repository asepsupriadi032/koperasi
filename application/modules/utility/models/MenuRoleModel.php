<?php
class MenuRoleModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        // $this->load->model('utility/LogModel');
		$this->container['data'] = null;
        $this->output = array();	
		$this->tokenAPI = new TokenAPI();
    }

    public function getUser($obj){
        $output=array();
        $token = $obj['token'];
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->where("mnu_user.ust_id !=","1");
            $data = $this->db->get('mnu_user')->result();

            if(!empty($obj['usr_id'])){
                $this->db->where('usr_id', $obj['usr_id']);
                $data = $this->db->get('mnu_user')->row();
            }

            $output=array("type" => "success", "row" => $data);

        }
        else{
            $output = array("type" => "error", "msg" => "Invalid token!");
        }
        return $output;
    }

    public function getAllMenu($obj){
        $usrId=$obj['usr_id'];
        $array = array();
        $token = $obj['token'];
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            //batas
            $sql1 = "SELECT  'aa' as is_link, '0' as level_menu, mnu_menu.mnu_id, mnu_menu.mnu_parent, LTRIM(RTRIM(mnu_menu.mnu_name)) as nama_menu, 
                    LTRIM(RTRIM(mnu_menu.mnu_url)) as url_menu, mnu_menu.mnu_urut, mnu_menu.mnu_status, mnu_menu_role.usr_id, mnu_menu_role.mrl_id
                    from mnu_menu left join mnu_menu_role on mnu_menu_role.mnu_id=mnu_menu.mnu_id and usr_id='".$usrId."'
                    where mnu_menu.mnu_parent='0' and mnu_menu.mnu_status='1' order by mnu_menu.mnu_urut asc";
            $data1 = $this->db->query($sql1)->result();
            foreach ($data1 as $arr1) {
                
                $array[] = array('id_menu' => $arr1->mnu_id, 
                    'level_menu' => $arr1->level_menu, 
                    'parent_menu' => $arr1->mnu_parent, 
                    'nama_menu' => $arr1->nama_menu, 
                    'url_menu' => $arr1->url_menu,
                    'usr_id' => $arr1->usr_id,
                    'is_link' => $arr1->is_link
                );                        
                
                $sql2 = "SELECT '0' as is_link, '1' as level_menu, mnu_menu.mnu_id, mnu_menu.mnu_parent, LTRIM(RTRIM(mnu_menu.mnu_name)) as nama_menu, 
                    LTRIM(RTRIM(mnu_menu.mnu_url)) as url_menu, mnu_menu.mnu_urut, mnu_menu.mnu_status, mnu_menu_role.usr_id, mnu_menu_role.mrl_id
                    from mnu_menu left join mnu_menu_role on mnu_menu_role.mnu_id=mnu_menu.mnu_id and usr_id='".$usrId."'
                    where mnu_menu.mnu_parent='".$arr1->mnu_id."' and mnu_menu.mnu_status='1' order by mnu_menu.mnu_urut asc";
                $data2 = $this->db->query($sql2)->result();
                
                foreach ($data2 as $arr2) {
                    $array[] = array('id_menu' => $arr2->mnu_id, 
                        'level_menu' => $arr2->level_menu, 
                        'parent_menu' => $arr2->mnu_parent, 
                        'nama_menu' => $arr2->nama_menu, 
                        'url_menu' => $arr2->url_menu,
                        'usr_id' => $arr2->usr_id,
                        'is_link' => $arr2->is_link
                    );

                    $sql3 = "SELECT '0' as is_link, '2' as level_menu, mnu_menu.mnu_id, mnu_menu.mnu_parent, LTRIM(RTRIM(mnu_menu.mnu_name)) as nama_menu, 
                        LTRIM(RTRIM(mnu_menu.mnu_url)) as url_menu, mnu_menu.mnu_urut, mnu_menu.mnu_status, mnu_menu_role.usr_id, mnu_menu_role.mrl_id
                        from mnu_menu left join mnu_menu_role on mnu_menu_role.mnu_id=mnu_menu.mnu_id and usr_id='".$usrId."'
                        where mnu_menu.mnu_parent='".$arr2->mnu_id."' and mnu_menu.mnu_status='1' order by mnu_menu.mnu_urut asc";
                    $data3 = $this->db->query($sql3)->result();
                    
                    foreach ($data3 as $arr3) {
                        $array[] = array('id_menu' => $arr3->mnu_id, 
                            'level_menu' => $arr3->level_menu, 
                            'parent_menu' => $arr3->mnu_parent, 
                            'nama_menu' => $arr3->nama_menu, 
                            'url_menu' => $arr3->url_menu,
                            'usr_id' => $arr3->usr_id,
                            'is_link' => $arr3->is_link
                        );

                //         $sql4 = "SELECT case when charindex('/', mnu_menu.url_menu) > 0  then 1 else 0 END as is_link, '3' as level_menu, mnu_menu.id_menu, mnu_menu.parent_menu, LTRIM(RTRIM(mnu_menu.nama_menu)) as nama_menu, 
                //             LTRIM(RTRIM(mnu_menu.url_menu)) as url_menu, mnu_menu.urutan, mnu_menu.status_aktif, mnu_menu_role.ust_id, mnu_menu_role.mrl_id,
                //             mnu_menu_role.mrl_create, mnu_menu_role.mrl_read, mnu_menu_role.mrl_update, mnu_menu_role.mrl_delete
                //             from mnu_menu left join mnu_menu_role on mnu_menu_role.id_menu=mnu_menu.id_menu and ust_id='".$ustId."'
                //             where mnu_menu.parent_menu='".$arr3->id_menu."' and mnu_menu.status_aktif='1' order by mnu_menu.urutan asc";
                //         $data4 = $this->db->query($sql4)->result();
                         
                //         foreach ($data4 as $arr4) {
                //             $array[] = array('id_menu' => $arr4->id_menu, 
                //                 'level_menu' => $arr4->level_menu, 
                //                 'parent_menu' => $arr4->parent_menu, 
                //                 'nama_menu' => $arr4->nama_menu, 
                //                 'url_menu' => $arr4->url_menu,
                //                 'ust_id' => $arr4->ust_id,
                //                 'mrl_create' => $arr4->mrl_create,
                //                 'mrl_read' => $arr4->mrl_read,
                //                 'mrl_update' => $arr4->mrl_update,
                //                 'mrl_delete' => $arr4->mrl_delete,
                //                 'is_link' => $arr4->is_link
                //             );
                                    
                //         }
                                
                    }
                            
                }
                
            }

            $data = $array;
        //batas
        $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        // var_dump($data); exit();

        return $output;
    }

    public function getAllProduk($obj){
        $usrId=$obj['usr_id'];
        $array = array();
        $token = $obj['token'];
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->select("kat_produk.*, mnu_produk_role.usr_id");
            $this->db->join("mnu_produk_role","mnu_produk_role.kat_id=kat_produk.kat_id","left");
            $data1 = $this->db->get("kat_produk")->result();
             // $this->db->query($sql1)->result();
            foreach ($data1 as $arr1) {
                
                $array[] = array('kat_id' => $arr1->kat_id, 
                    'kat_kode' => $arr1->kat_kode, 
                    'usr_id' => $arr1->usr_id, 
                    'kat_nama' => $arr1->kat_nama
                ); 
            }
            $data = $array;
            // var_dump($data); exit();
        //batas
        $output=array("type" => "success", "row" => $data);

        }else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        // var_dump($data); exit();

        return $output;
    }

    public function postMenuRole($obj){
        $output=array();
        $token = $obj['token'];
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
            $action = "Insert";
            $this->db->where("usr_id", $obj['usr_id']);
            $this->db->delete("mnu_menu_role");
            
            $this->db->where("usr_id", $obj['usr_id']);
            $this->db->delete("mnu_produk_role");

            $jumlah = $obj['jumlah'];
            for($x = 1; $x <= $jumlah; $x++) {
                $mrl_access = "mrl_access".$x;  
                $id_menu = "id_menu".$x;

                if(!empty($obj[$mrl_access])) {
                    $this->db->set('usr_id',$obj['usr_id']);
                    $this->db->set('mnu_id', $obj[$id_menu]);
                    $exec = $this->db->insert('mnu_menu_role');
                }
            }

            $jumlahProduk = $obj['jumlahProduk'];

            for($y = 1; $y <= $jumlahProduk; $y++) {
                $prod_access = "prod_access".$y;  
                $kat_id = "kat_id".$y;
                if(!empty($obj[$prod_access])) {
                    $this->db->set('usr_id',$obj['usr_id']);
                    $this->db->set('kat_id', $obj[$kat_id]);
                    $exec = $this->db->insert('mnu_produk_role');
                }
            }

            if($exec) {
                $output=array("type" => "success", "msg" => "Request success, data saved!");
            }
            else {
                $output=array("type" => "error", "msg" => "Request failed, please check the data!");
            }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function delInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->where('ins_id', $obj->id);
            $exec = $this->db->delete('ins_instansi');

            if($exec) {
                
                $output= array("type" => "success", "msg" => "Request success, data deleted!");
            }
            else {
                $output= array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }
        else{
            $output= array("type" => "error", "msg" => "Request failed, invalid token!");
        }

        return $output;
    }

}