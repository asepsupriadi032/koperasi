<?php
class Menu_role extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;

		
		$this->API = $this->config->item('api_url')."/utility";	
		$this->tokenAPI = new TokenAPI();
    }

    public function getParent0($ustId = NULL){

        $param["action"] ="parent0";
        $param['ust_id']=$ustId;
        $param['token'] = $this->tokenAPI->getToken();


        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
         //var_dump($data);
        //exit();
        $parent0 = 0;
        if(!empty($data->row)){
            $parent0 = $data->row; 
        }
        
        
        return $parent0;
    }

    public function getParent1($id_menu = null, $ustId = NULL){

        $param['id_menu'] = $id_menu;
        $param['ust_id'] = $ustId;
        $param["action"] ="parent1";
        $param['token'] = $this->tokenAPI->getToken();

        /*
        echo $this->API.'/Menu_role'."<br/>";
        echo "<pre>";
        print_r($param);
        echo "</pre>";
        exit();
        */

        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
        //var_dump($id_menu);
        //exit();
        $parent1 = 0;
        if(!empty($data->row)){
            $parent1= $data->row; 
        }
        return $parent1;
    }

    public function getParent2($id_menu=null, $ustId = NULL){

        $param['id_menu']=$id_menu;
        $param['ust_id']=$ustId;
        $param["action"] ="parent2";
        $param['token'] = $this->tokenAPI->getToken();

        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
        // var_dump($data);
        // exit();
        $parent2 = 0;
        if(!empty($data->row)){
            $parent2= $data->row; 
        }
        
        return $parent2;
    }

    public function getMenu($id_menu = 0, $ustId = NULL){

        $param['id_menu'] = $id_menu;
        $param['ust_id'] = $ustId;
        $param["action"] ="get_menu";
        $param['token'] = $this->tokenAPI->getToken();

        /*
        echo $this->API.'/Menu_role';
        echo "<pre>";
        print_r($param);
        echo "</pre>";
        */
        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
        $output = (object) array("jumlah" => $data->jumlah, "data" => $data->row);

        //var_dump($id_menu);
        //exit();
        /*
        $parent1 = 0;
        if(!empty($data->row)){
            $parent1= $data->row; 
        }
        */
        return $output;
    }

    
    public function getCrud($ust_id=NULL){

        $param['ust_id']=$ust_id;
        $param["action"] ="getCrud";
        $param['token'] = $this->tokenAPI->getToken();

        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
        // var_dump($data);
        // exit();
        $crud = 0;
        if(!empty($data->row)){
            $crud= $data->row; 
        }
        
        return $crud;

    }

    public function getLevel1($id_menu=NULL, $ust_id=NULL){

        $param['id_menu']=$id_menu;
        $param['ust_id']=$ust_id;
        $param["action"] ="getLevel1";
        $param['token'] = $this->tokenAPI->getToken();

        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
        // var_dump($data);
        // exit();
        $level1 = 0;
        if(!empty($data->row)){
            $level1= $data->row; 
        }
        
        return $level1;

    }

    public function getLevel2($id_menu=NULL, $ust_id=NULL){

        $param['id_menu']=$id_menu;
        $param['ust_id']=$ust_id;
        $param["action"] ="getLevel2";
        $param['token'] = $this->tokenAPI->getToken();

        $data = json_decode($this->curl->simple_get($this->API.'/Menu_role', $param));
        // var_dump($data);
        // exit();
        $level2 = 0;
        if(!empty($data->row)){
            $level2= $data->row; 
        }
        
        return $level2;

    }


}