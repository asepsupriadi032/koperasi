<?php
class SubInstansiModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
        // $this->load->model('utility/LogModel');
		$this->container['data'] = null;
        $this->output = array();	
		$this->tokenAPI = new TokenAPI();
    }

    public function getKatInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        $data = $this->db->get('ins_instansi')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function getSubInstansi($obj){
        $output=array();
        $tokenValid = true;

        if($tokenValid){
            $this->db->join('ins_instansi','ins_instansi.ins_id=ins_sub_instansi.ins_id');
            $data = $this->db->get('ins_sub_instansi')->result();

           
            if(!empty($obj->id)){
                $this->db->where('ins_sub_instansi.sin_id', $obj->id);
                $this->db->join('ins_instansi','ins_instansi.ins_id=ins_sub_instansi.ins_id');
                $data = $this->db->get('ins_sub_instansi')->row();
            }
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postSubInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
                $this->db->set("ins_id", $obj->ins_id);
                $this->db->set("sin_nama", $obj->sin_nama);
                $this->db->set("sin_kode", $obj->sin_kode);
                $this->db->set("sin_alamat", $obj->sin_alamat);
                $this->db->set("sin_crdt", date('Y-m-d H:i:s'));
                if(!empty($obj->sin_id)) {
                    $action = "Update";
     
                    $this->db->where('sin_id', $obj->sin_id);
                    $this->db->set("sin_chby", $this->session->userdata("hcmUser"));
                    $this->db->set("sin_chdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->update("ins_sub_instansi");
                }
                else {
                    $action = "Insert";
                    $this->db->set("sin_crby", $this->session->userdata("hcmUser"));
                    $this->db->set("sin_crdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->insert('ins_sub_instansi');     
                }

                if($exec) {
                     //Save Log Db
                    $modul = "Sub Instansi";
                    $usr_id = $this->session->userdata("hcmIdUser");
                    $valid = $this->addLog($action, $modul, "ins_sub_instansi", $obj, $usr_id);
                    // End Save Log
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function delSubInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){
            $action = "Delete";
            //save log
            $modul = "Sub Instansi";
            $usr_id = $this->session->userdata("hcmIdUser");
            $valid = $this->addLog($action, $modul, "ins_sub_instansi", array("sin_id" => $obj->id), $usr_id);
            //and save log
            $this->db->where('sin_id', $obj->id);
            $exec = $this->db->delete('ins_sub_instansi');

            if($exec) {
                
                $output= array("type" => "success", "msg" => "Request success, data deleted!");
            }
            else {
                $output= array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }
        else{
            $output= array("type" => "error", "msg" => "Request failed, invalid token!");
        }

        return $output;
    }

     public function addLog($act, $modul, $table, $record, $id_user = NULL){
        $log = $this->session->all_userdata();
        $valid = false;

        if(empty($id_user)){
            $id_user = $this->session->userdata("hcmIdUser");
        }

        if ($act == "Delete") {
            $field = array();
            $data = array();

            $selFields = $this->db->field_data($table);
            $c = 0;
            foreach ($selFields as $d) {
               $c++;
               $field[$c] = $d->name;
            }

            $query = $this->db->get_where($table, $record);
            $data = $query->row();

            $arrData = array_combine($field, (array) $data);
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }
        else {
            $arrData = (array) $record;
            $dataRecord = str_replace("+", " ", http_build_query($arrData, '', ', '));
        }

        $splitBy = explode(", token", $dataRecord);
        $dataRecord = $splitBy[0];

        $this->db->set("usr_id", $id_user);
        $this->db->set("log_action", $act);
        $this->db->set("log_date", date("Y-m-d H:i:s"));
        $this->db->set("log_record", $dataRecord);
        $this->db->set("log_table", $table);
        $this->db->set("log_modul", $modul);
        $valid = $this->db->insert('mnu_log');

        return $valid;
    }

}