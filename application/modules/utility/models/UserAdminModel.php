<?php
class UserAdminModel extends CI_Model {
	private $container;
	private $valid = false;
	private $API;
	private $tokenAPI;
    var $output;
    function __construct(){
        parent::__construct();
        $this->load->helper('accesscontrol');
		$this->load->helper('token');		
		$this->load->helper('url');	
		$this->container['data'] = null;
        $this->output = array();	
		$this->tokenAPI = new TokenAPI();
    }

    public function getUserType($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        $this->db->where("mnu_user_type.ust_id !=","1");
        $data = $this->db->get('mnu_user_type')->result();              
        $output=array("type" => "success", "row" => $data);
        return $output;
    }

    public function getUserAdmin($obj){
        $output=array();
        $tokenValid = true;

        if($tokenValid){

            $this->db->where("mnu_user.ust_id !=","1");
            $this->db->join('mnu_user_type','mnu_user_type.ust_id=mnu_user.ust_id', 'left');
            $data = $this->db->get('mnu_user')->result();

           
            if(!empty($obj->id)){
                $this->db->where('mnu_user.usr_id', $obj->id);
                $this->db->join('mnu_user_type','mnu_user_type.ust_id=mnu_user.ust_id');
                $data = $this->db->get('mnu_user')->row();
            }
            // var_dump($data); exit();
            $output=array("type" => "success", "row" => $data);

        }
        else{
            $this->response(array("type" => "error", "msg" => "Invalid token!"));
        }
        return $output;
    }

    public function postUserAdmin($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);
        if($tokenValid){
                $this->db->set("ust_id", "2");
                $this->db->set("usr_name", $obj->usr_name);
                $this->db->set("usr_username", $obj->usr_username);
                $this->db->set("usr_email", $obj->usr_email);
                $this->db->set("usr_hp", $obj->usr_hp);
                $this->db->set("usr_status", $obj->usr_status);
                $this->db->set("usr_crdt", date('Y-m-d H:i:s'));
                if(!empty($obj->usr_id)) {
                    $action = "update";
                        if(!empty($obj->usr_password)){

                            $this->db->set("usr_password", md5($obj->usr_password));
                        }
                    $this->db->where('usr_id', $obj->usr_id);
                    $this->db->set("usr_chdt", date('Y-m-d H:i:s'));
                    $exec = $this->db->update("mnu_user");
                }
                else {
                    
                    $this->db->set("usr_password", md5($obj->usr_password));
                    $exec = $this->db->insert('mnu_user');     
                }

                if($exec) {
                    $output=array("type" => "success", "msg" => "Request success, data saved!");
                }
                else {
                    $output=array("type" => "error", "msg" => "Request failed, please check the data!");
                }

                //Save Log Db
                //$valid = $this->logUpdate->addLog($action, "step02_kebutuhandokumen", $args);
                // End Save Log
            }
            else{
                $output=array("type" => "error", "msg" => "Request failed, invalid token!");
            }
            return $output;

    }

    public function delSubInstansi($obj){
        $output=array();
        $token = $obj->token;
        $tokenValid = $this->tokenAPI->checkToken($token);

        if($tokenValid){

            $this->db->where('sin_id', $obj->id);
            $exec = $this->db->delete('ins_sub_instansi');

            if($exec) {
                
                $output= array("type" => "success", "msg" => "Request success, data deleted!");
            }
            else {
                $output= array("type" => "error", "msg" => "Request failed, please check the data!");
            }
        }
        else{
            $output= array("type" => "error", "msg" => "Request failed, invalid token!");
        }

        return $output;
    }

}