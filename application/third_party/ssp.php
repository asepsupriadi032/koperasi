<?php
// HERE I have appended the modified SSP.PHP source fragments EVERYTHING WORKS
//
 
/**
* Paging
*/
static function limit ( $request, $columns ) {
 $limit = '';
 if ( isset($request['start']) && $request['length'] != -1 ) {
  $limit = "ORDER BY [LINE] OFFSET ".intval($request['start'])." ROWS FETCH NEXT ".intval($request['length'])." ROWS ONLY";
  }
// limit and order conflict when using sql server.
// so duplicate the functionality in ORDER and switch on/off as needed based on ORDER
  if ( isset($request['order'])) {
   $limit = '';    // if there is an ORDER request then clear the limit
   return $limit;    // because the ORDER function will handle the LIMIT
  }
  else
  {
  return $limit;
  }
}
 
/**
* Ordering
*/
static function order ( $request, $columns ) {
  $order = '';
  if ( isset($request['order']) && count($request['order']) ) {
    $orderBy = array();
    $dtColumns = self::pluck( $columns, 'dt' );
    for ( $i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
      // Convert the column index into the column data property
      $columnIdx = intval($request['order'][$i]['column']);
      $requestColumn = $request['columns'][$columnIdx];
      $columnIdx = array_search( $requestColumn['data'], $dtColumns );
      $column = $columns[ $columnIdx ];
      if ( $requestColumn['orderable'] == 'true' ) {
        $dir = $request['order'][$i]['dir'] === 'asc' ?
         'ASC' :
         'DESC';
         $orderBy[] = '['.$column['db'].'] '.$dir;   // revised for SQL Server
      }
    }
  // see "static function limit" above to explain the next line.
  $order =  "ORDER BY ".implode(', ', $orderBy)." OFFSET ".intval($request['start'])." ROWS FETCH NEXT ".intval($request['length'])." ROWS ONLY";
  }
  return $order;
}
 
static function simple ( $request, $sql_details, $table, $primaryKey, $columns ) {
  $bindings = array();
  $db = self::sql_connect( $sql_details );
  // Build the SQL query string from the request
  $limit = self::limit( $request, $columns );
  $order = self::order( $request, $columns );
  $where = self::filter( $request, $columns, $bindings );
 
// Main query to actually get the data
  $data = self::sql_exec( $db, $bindings,
        "SET NOCOUNT ON SELECT ".implode(", ", self::pluck($columns, 'db'))." FROM $table $where $order $limit" );
  
// Data set length after filtering  the $where will update info OR will be blank when not doing a search
  $resFilterLength = self::sql_exec( $db, $bindings,
        "SET NOCOUNT ON SELECT ".implode(", ", self::pluck($columns, 'db'))." FROM $table $where " );
  $recordsFiltered = count($resFilterLength);
 
  // Total data set length
  $resTotalLength = self::sql_exec( $db,"SET NOCOUNT ON SELECT COUNT({$primaryKey}) FROM $table" );
  $recordsTotal = $resTotalLength[0][0];
          
  /*  Output   */
  return array(
    "draw"            => intval( $request['draw'] ),
    "recordsTotal"    => intval( $recordsTotal ),
    "recordsFiltered" => intval( $recordsFiltered ),
    "data"            => self::data_output( $columns, $data )
    );
}
?>