<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$root = "http://".$_SERVER['HTTP_HOST']; 
$root .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html>
<head>
         <title>KOPERASI TASS INDONESIA NUSANTARA</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
<link href="<?php echo $root ?>assets/css/adminstyles.css" rel="stylesheet" type="text/css">
 <link rel="shortcut icon" href="<?php echo $root ?>assets/global/img/favicon.png" />
<script type="text/javascript" src="<?php echo $root ?>assets/js/vendors/modernizr/modernizr.custom.js"></script>
</head>

<body>
  <center>
    <div class="standalone-page-wrapper"> 
      
      <!--Top Block-->
      
      <!--/Top Block--> 
      <!--Bottom Block-->
      <div class="error-bottom-block">
        <div class="col-md-6 col-md-offset-3 error-description">
          <br/>
          <img src="<?php echo base_url() ?>assets/global/img/under-maintenance.jpg" width="50%">
          <br/>
          <br/>
          <div class="todo">
          </div>

          <div class="copyrights"> <a href="<?php echo $root ?>">Kembali Ke Depan</a></div>

        </div>
      </div>
      <!--/Bottom Block--> 
    </div>
  </center>
</body>
</html>
