/*
Initial Confirmation Setup, menggunakan TOASTR
*/
var UIToastr = function () {
    return {
        init: function () {            
            toastr.options = {
                  "closeButton": true,
              "debug": false,
              "positionClass": "toast-top-right",
              "onclick": null,
              "showDuration": "0",
              "hideDuration": "1000",
              "timeOut": "3500",
              "extendedTimeOut": "0",
              "showEasing": "swing",
              "hideEasing": "linear",
              "showMethod": "fadeIn",
              "hideMethod": "fadeOut"
            }           
        }
    };

}();
/*
END Initial Confirmation
*/



$("form").submit(function(event) {
  blockPage();
});

function blockPage(){
    App.blockUI({
        boxed: true
    });
}

function ublockPage(){
    App.unblockUI();
}

jQuery(document).ready(function() { 
    UIToastr.init();
});

